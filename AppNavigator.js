import React, { Component } from 'react';
import { StyleSheet, Text, Image, View, Platform, TouchableOpacity } from 'react-native';

import { Provider, connect } from 'react-redux';

import axios from 'axios';
import axiosMiddleware from 'redux-axios-middleware';

const client = axios.create({
  baseURL: 'https://api.tucalificas.app/',
  // baseURL: 'http://upc.josegomezr.com.ve/',
  // baseURL: 'http://192.168.1.103:7100/',
  baseURL: 'http://192.168.0.3:8000/',
  // baseURL: 'http://10.42.0.1:8000/',
  responseType: 'json',
});

import { createStore, applyMiddleware } from 'redux';

import reducer from './redux/reducer';

const store = createStore(reducer, applyMiddleware(axiosMiddleware(client)));
import { fontLoaded } from './redux/actions.js'

import { Router, Scene, Actions, Tabs } from 'react-native-router-flux';
const RouterWithRedux = connect()(Router);

import ListaCursos from './views/main/Start';
import Splash from './views/Splash';
import ListaProfesores from './views/cursos/CursoSeleccionado';
import PerfilProfesorCurso from './views/cursos/ProfesorCurso';
import ComentariosProfesorCurso from './views/cursos/ComentariosProfesorCurso';
import BioProfesor from './views/cursos/BioProfesor';
import RecordEstudiante from './views/perfil/Record';

import * as constants from './constants'

import {Asset} from 'expo'

import {getMensajes} from './redux/actions'


var navOpts = {
  title: '',
  headerBackTitle: null,
  headerTruncatedBackTitle: null,
  headerTransparent: true,
  headerMode: 'float',
  headerStyle: {
    elevation: 0,
    borderBottomWidth: 0,
    borderBottomColor: 'transparent',
  },
  renderBackButton: () => (
    <TouchableOpacity onPress={() => Actions.pop() } style={{
      backgroundColor: 'white',
      borderRadius: 24,
      shadowColor: 'rgba(116, 116, 116, 0.25)',
      shadowOpacity: 1,
      shadowOffset: {
        height: 4,
      },
      shadowRadius: 4,
      elevation: 4,
      padding: 8,
      marginLeft: 16,
    }}>
      <Image
        source={constants.ICON_BACK}
        resizeMode="stretch"
        style={{
          height: 20,
          width: 20,
          ...Platform.select({
            ios:{
              width: 24,
              height: 24,
            }
          })
        }}
      />
    </TouchableOpacity>
  ),
}

import Login from './views/main/Login'
import Exito from './views/Exito'
import ErrorView from './views/Error'
import Ranking from './views/Ranking'

import Calificar from './views/cursos/Calificar'
import MainMensajes from './views/mensajeria/MainMensajes'
import VerHilo from './views/mensajeria/VerHilo'
import MainPerfil from './views/perfil/MainPerfil'
import Feedback from './views/perfil/Feedback'
import NuevoMensaje from './views/mensajeria/NuevoMensaje'

import { Font, AppLoading } from 'expo';

const tabIcon = (name, focused) => {
  var suffix =focused ? 'COLOR' : 'OFF';
  var itemName = `${name}_${suffix}`;
  return (<Image
    source={constants[itemName]}
    resizeMode="stretch"
    style={{
      height: 20,
      width: 20,
    }}
  />
)
};

const labelIfFocused = (name) => {
  return name
}


export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <RouterWithRedux>
          <Scene key="main">
            {/*
              @todo
              <Scene key="splash"
              component={Login}  header={null}
              />
              
              * LOGIN
            */}

            {/* <Scene key="recordEstudiante" header={null} component={RecordEstudiante} /> */}

            <Scene key="splash" component={Splash} header={null} />
            <Scene key="login" component={Login} header={null} />


            <Scene key="authFlow" tabs={true} tabBarPosition={"bottom"}
              swipeEnabled={false} {...navOpts}>

              {/* INICIO - PRIMERA TAB */}
              <Scene key="calificarFlow" tabBarLabel={labelIfFocused("Calificar")} title="Calificar"
                icon={({ focused }) => tabIcon('ICONO_BOOK', focused)}
                {...navOpts}

              >
                <Scene key="listaCursos" header={null} component={ListaCursos} />
                <Scene key="listaProfesores" component={ListaProfesores} />
                <Scene key="perfilProfesorCurso"
                  component={PerfilProfesorCurso} />

                <Scene key="calificar" component={Calificar} />

                <Scene key="fullComments" component={ComentariosProfesorCurso} />
                <Scene key="sendMessage" component={NuevoMensaje} />
                <Scene key="success" component={Exito} />
                <Scene key="errorKitty" component={ErrorView} />
                <Scene key="bioProfesor" component={BioProfesor} />
              </Scene>
              {/* FIN PRIMERA TAB */}

              {/* INICIO SEGUNDA TAB */}
              <Scene key="rankingFlow" tabBarLabel={labelIfFocused("Ranking")} title="Ranking"
                icon={({ focused }) => tabIcon('ICONO_MEDALLA', focused)}
                {...navOpts}>
                <Scene header={null} key="mainRanking" component={Ranking} />
              </Scene>
              {/* FIN SEGUNDA TAB */}

              {/* INICIO TERCERA TAB */}
              <Scene key="chatFlow" title="Mensajes" tabBarLabel={labelIfFocused("Mensajes")}
                icon={({ focused }) => tabIcon('ICONO_DIPLOMA', focused)}
                {...navOpts}>
                <Scene key="mainMensajes" component={MainMensajes} header={null} />
                <Scene key="viewChat" component={VerHilo} onExit={() => {
                  var correo = store.getState().estudiante_actual.estudiante_correo
                  console.log(correo)
                  store.dispatch(getMensajes(correo))
                }} />
              </Scene>
              {/* FIN TERCERA TAB */}

              {/* INICIO CUARTA TAB */}
              <Scene key="perfilFlow" title="Perfil" tabBarLabel={labelIfFocused("Perfil")}
                icon={({ focused }) => tabIcon('ICONO_BIRRETE', focused)}
                {...navOpts}>
                <Scene key="mainPerfil" header={null} component={MainPerfil} />
                <Scene key="feedback" header={null} component={Feedback} />
                <Scene key="recordEstudiante" header={null} component={RecordEstudiante} />
              </Scene>
              {/* FIN CUARTA TAB */}

            </Scene>
            {/* FIN TABS */}
          </Scene>
        </RouterWithRedux>
      </Provider>
    );
  }
}