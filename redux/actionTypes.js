export const CLEAR_SIDE_EFFECT = 'upc/hack/CLEAR_SE';
export const SET_LOAD_LOGIN_SE = 'upc/hack/LOAD_LOGIN_SE';
export const SET_RELOAD_PFPROF = 'upc/hack/RELOAD_PROFILE_PROFESOR';
export const STORE_STUDENT = 'upc/hack/STORE_STUDENT';
export const SET_LOADING = 'upc/hack/SET_LOADING';
export const FONT_LOADED = 'upc/hack/FONT_LOADED';

export const GET_CURSOS = 'upc/cursos/LOAD';
export const GET_CURSOS_SUCCESS = 'upc/cursos/LOAD_SUCCESS';
export const GET_CURSOS_FAIL = 'upc/cursos/LOAD_FAIL';

export const GET_DISTRITOS = 'upc/distritos/LOAD';
export const GET_DISTRITOS_SUCCESS = 'upc/distritos/LOAD_SUCCESS';
export const GET_DISTRITOS_FAIL = 'upc/distritos/LOAD_FAIL';

export const GET_CURSOS_APROBADOS = 'upc/record/LOAD';
export const GET_CURSOS_APROBADOS_SUCCESS = 'upc/record/LOAD_SUCCESS';
export const GET_CURSOS_APROBADOS_FAIL = 'upc/record/LOAD_FAIL';

export const GET_PROFESORES = 'upc/profesores/LOAD';
export const GET_PROFESORES_SUCCESS = 'upc/profesores/LOAD_SUCCESS';
export const GET_PROFESORES_FAIL = 'upc/profesores/LOAD_FAIL';

export const GET_PERFIL_PROFESOR = 'upc/perfil_profesor/LOAD';
export const GET_PERFIL_PROFESOR_SUCCESS = 'upc/perfil_profesor/LOAD_SUCCESS';
export const GET_PERFIL_PROFESOR_FAIL = 'upc/perfil_profesor/LOAD_FAIL';

export const GET_PERFIL_ESTUDIANTE = 'upc/perfil_estudiante/LOAD';
export const GET_PERFIL_ESTUDIANTE_SUCCESS = 'upc/perfil_estudiante/LOAD_SUCCESS';
export const GET_PERFIL_ESTUDIANTE_FAIL = 'upc/perfil_estudiante/LOAD_FAIL';

export const GET_MENSAJES = 'upc/mensajeria/LOAD';
export const GET_MENSAJES_SUCCESS = 'upc/mensajeria/LOAD_SUCCESS';
export const GET_MENSAJES_FAIL = 'upc/mensajeria/LOAD_FAIL';

export const GET_PREGUNTAS = 'upc/preguntas/LOAD';
export const GET_PREGUNTAS_SUCCESS = 'upc/preguntas/LOAD_SUCCESS';
export const GET_PREGUNTAS_FAIL = 'upc/preguntas/LOAD_FAIL';

export const GET_HASHTAGS = 'upc/hashtags/LOAD';
export const GET_HASHTAGS_SUCCESS = 'upc/hashtags/LOAD_SUCCESS';
export const GET_HASHTAGS_FAIL = 'upc/hashtags/LOAD_FAIL';

export const GET_GRADOS = 'upc/grados/LOAD';
export const GET_GRADOS_SUCCESS = 'upc/grados/LOAD_SUCCESS';
export const GET_GRADOS_FAIL = 'upc/grados/LOAD_FAIL';

export const GET_CARRERAS = 'upc/carreras/LOAD';
export const GET_CARRERAS_SUCCESS = 'upc/carreras/LOAD_SUCCESS';
export const GET_CARRERAS_FAIL = 'upc/carreras/LOAD_FAIL';

export const GET_RANK_CURSO = 'upc/rank_curso/LOAD';
export const GET_RANK_CURSO_SUCCESS = 'upc/rank_curso/LOAD_SUCCESS';
export const GET_RANK_CURSO_FAIL = 'upc/rank_curso/LOAD_FAIL';

export const GET_RANK_RECOMENDADO = 'upc/rank_recomendado/LOAD';
export const GET_RANK_RECOMENDADO_SUCCESS = 'upc/rank_recomendado/LOAD_SUCCESS';
export const GET_RANK_RECOMENDADO_FAIL = 'upc/rank_recomendado/LOAD_FAIL';

export const GET_RANK_VOTADO = 'upc/rank_votado/LOAD';
export const GET_RANK_VOTADO_SUCCESS = 'upc/rank_votado/LOAD_SUCCESS';
export const GET_RANK_VOTADO_FAIL = 'upc/rank_votado/LOAD_FAIL';

export const CALIFICAR = 'upc/calificar/LOAD';
export const CALIFICAR_SUCCESS = 'upc/calificar/LOAD_SUCCESS';
export const CALIFICAR_FAIL = 'upc/calificar/LOAD_FAIL';

export const START_CHAT = 'upc/start_chat/LOAD';
export const START_CHAT_SUCCESS = 'upc/start_chat/LOAD_SUCCESS';
export const START_CHAT_FAIL = 'upc/start_chat/LOAD_FAIL';

export const GET_COMMENTS = 'upc/get_comments/LOAD';
export const GET_COMMENTS_SUCCESS = 'upc/get_comments/LOAD_SUCCESS';
export const GET_COMMENTS_FAIL = 'upc/get_comments/LOAD_FAIL';

export const GET_CHAT = 'upc/get_chat/LOAD';
export const GET_CHAT_SUCCESS = 'upc/get_chat/LOAD_SUCCESS';
export const GET_CHAT_FAIL = 'upc/get_chat/LOAD_FAIL';

export const SEND_MSG = 'upc/send_msg/LOAD';
export const SEND_MSG_SUCCESS = 'upc/send_msg/LOAD_SUCCESS';
export const SEND_MSG_FAIL = 'upc/send_msg/LOAD_FAIL';

export const CREATE_NEW_CHAT = 'upc/CREATE_NEW_CHAT/LOAD';
export const CREATE_NEW_CHAT_SUCCESS = 'upc/CREATE_NEW_CHAT/LOAD_SUCCESS';
export const CREATE_NEW_CHAT_FAIL = 'upc/CREATE_NEW_CHAT/LOAD_FAIL';

export const LOGIN = 'upc/LOGIN';
export const LOGIN_SUCCESS = 'upc/LOGIN_SUCCESS';
export const LOGIN_FAIL = 'upc/LOGIN_FAIL';

export const EDITAR_PERFIL = 'upc/EDITAR_PERFIL';
export const EDITAR_PERFIL_SUCCESS = 'upc/EDITAR_PERFIL_SUCCESS';
export const EDITAR_PERFIL_FAIL = 'upc/EDITAR_PERFIL_FAIL';

export const ENVIAR_FEEDBACK = 'upc/ENVIAR_FEEDBACK';
export const ENVIAR_FEEDBACK_SUCCESS = 'upc/ENVIAR_FEEDBACK_SUCCESS';
export const ENVIAR_FEEDBACK_FAIL = 'upc/ENVIAR_FEEDBACK_FAIL';

export const SET_CURSO_ACTUAL = 'upc/setters/CURSO'
export const SET_PROFESOR_ACTUAL = 'upc/setters/PROFESOR'
export const SET_CURRENT_LOGIN = 'upc/setters/LOGIN'
export const SET_COMENTARIO_ACTUAL = 'upc/setters/COMENTARIO'
export const SET_CHAT_ACTUAL = 'upc/setters/CHAT'