import * as ACTION from './actionTypes';

export function hackSetLoadLogin(user) {
    return {
        type: ACTION.SET_LOAD_LOGIN_SE
    };
}

export function clearSideEffects(user) {
    return {
        type: ACTION.CLEAR_SIDE_EFFECT
    };
}

export function setCursoActual(cursoObj) {
    return {
        type: ACTION.SET_CURSO_ACTUAL,
        payload:{
            data: cursoObj,
        }
    }
}

export function setProfesorActual(profObj) {
    return {
        type: ACTION.SET_PROFESOR_ACTUAL,
        payload:{
            data: profObj,
        }
    }
}

export function setLoading(loading){
    return {
        type: ACTION.SET_LOADING,
        payload: {
            loading
        }
    }
}

export function fontLoaded(){
    return {
        type: ACTION.FONT_LOADED,
    }
}

/*
request:{
    // where
    url: '/x/b/z',
    // how
    method: 'get' | 'post' | 'put' | 'delete',
    // querystring
    params: {
        //...
    }
    // payload
    data: { 
        // ...
    }
}
*/

export function login(email, password) {
    return {
        type: ACTION.LOGIN,
        payload: {
            request: {
                url: `/login-api`,
                method: 'post',
                data: {
                    email, password
                }
            }
        }
    };
}


export function storeLogin(estudiante_actual){
    return {
        type: ACTION.SET_CURRENT_LOGIN,
        payload: {
            data: estudiante_actual
        }
    };
}

function headerConfig(email_estudiante, apiToken = '') {
    return {
        headers: {
            'Intranet-Email': email_estudiante,
            'Api-Token': apiToken
        },
    }   
}

export function loadDistritos(email) {
    return {
        type: ACTION.GET_DISTRITOS,
        payload: {
            request: {
                url: `/distritos`,
                method: 'get',
                ...headerConfig(email)
            }
        }
    };
}

export function listCursos(email_estudiante) {
    return {
        type: ACTION.GET_CURSOS,
        payload: {
            request: {
                url: `/cursos`,
                ...headerConfig(email_estudiante)
            },
        }
    };
}

export function listCursosAprobados(email_estudiante) {
    return {
        type: ACTION.GET_CURSOS_APROBADOS,
        payload: {
            request: {
                url: `/record`,
                ...headerConfig(email_estudiante)
            },
        }
    };
}

export function listProfesores(email_estudiante, cursoId) {
    return {
        type: ACTION.GET_PROFESORES,
        payload: {
            request: {
                url: `/cursos/${cursoId}/profesores/`,
                ...headerConfig(email_estudiante)
            },
        }
    };
}

export function getPerfilProfesor(email_estudiante, curso_id, profesor_id) {
    return {
        type: ACTION.GET_PERFIL_PROFESOR,
        payload: {
            request: {
                url: `/cursos/${curso_id}/profesores/${profesor_id}`,
                ...headerConfig(email_estudiante)
            }
        }
    }
}

export function getPreguntas(email_estudiante) {
    return {
        type: ACTION.GET_PREGUNTAS,
        payload: {
            request: {
                url: `/preguntas`,
                ...headerConfig(email_estudiante)
            }
        }
    }
}

export function getHashtags(email_estudiante) {
    return {
        type: ACTION.GET_HASHTAGS,
        payload: {
            request: {
                url: `/hashtags`,
                ...headerConfig(email_estudiante)
            }
        }
    }
}

export function getGrados(email_estudiante) {
    return {
        type: ACTION.GET_GRADOS,
        payload: {
            request: {
                url: `/grados`,
                ...headerConfig(email_estudiante)
            }
        }
    }
}

export function getCarreras(email_estudiante) {
    return {
        type: ACTION.GET_CARRERAS,
        payload: {
            request: {
                url: `/carreras`,
                ...headerConfig(email_estudiante)
            }
        }
    }
}

export function calificar(email_estudiante, id_inscripcion, data) {
    return {
        type: ACTION.CALIFICAR,
        payload: {
            request: {
                method: 'post',
                url: `/inscripcion/${id_inscripcion}/calificar`,
                data: data,
                ...headerConfig(email_estudiante)
            }
        }
    }
}

export function getPerfil(email_estudiante) {
    return {
        type: ACTION.GET_PERFIL_ESTUDIANTE,
        payload: {
            request: {
                url: `/perfil`,
                ...headerConfig(email_estudiante)
            }
        }
    }
}

export function getMensajes(email_estudiante) {
    return {
        type: ACTION.GET_MENSAJES,
        payload: {
            request: {
                url: `/perfil/mensajes`,
                ...headerConfig(email_estudiante)
            }
        }
    }
}

export function setChatActual(chat){
    return {
        type: ACTION.SET_CHAT_ACTUAL,
        payload: {
            data: chat,
        }
    }
}

export function createNewChat(email_estudiante, comment){
    return {
        type: ACTION.CREATE_NEW_CHAT,
        payload: {
            request: {
                url: `/perfil/mensajes/nuevo`,
                method: 'POST',
                data: comment,
                ...headerConfig(email_estudiante)
            },
        }
    }
}

export function getConversacion(email_estudiante, conversacionId) {
    return {
        type: ACTION.GET_CHAT,
        payload: {
            request: {
                url: `/perfil/mensajes/${conversacionId}`,
                ...headerConfig(email_estudiante)
            }
        }
    }
}

export function rankingCurso(email_estudiante) {
    return {
        type: ACTION.GET_RANK_CURSO,
        payload: {
            request: {
                url: `/ranking/curso`,
                ...headerConfig(email_estudiante)
            }
        }
    }
}
export function rankingRecomendado(email_estudiante) {
    return {
        type: ACTION.GET_RANK_RECOMENDADO,
        payload: {
            request: {
                url: `/ranking/recomendado`,
                ...headerConfig(email_estudiante)
            }
        }
    }
}
export function rankingVotado(email_estudiante) {
    return {
        type: ACTION.GET_RANK_VOTADO,
        payload: {
            request: {
                url: `/ranking/mas-votado`,
                ...headerConfig(email_estudiante)
            }
        }
    }
}

export function getComentarios(email_estudiante, cursoId, profesorId) {
    return {
        type: ACTION.GET_COMMENTS,
        payload: {
            request:{
                url: `/cursos/${cursoId}/profesores/${profesorId}/comentarios`,
                ...headerConfig(email_estudiante)
            }
        },
    }
}

export function setComentarioActual(comentarioObj){
    return {
        type: ACTION.SET_COMENTARIO_ACTUAL,
        payload: {
            data: comentarioObj
        },
    }
}

export function iniciarConversacion(email_estudiante, inscripcion_id, obj){
    return {
        type: ACTION.START_CHAT,
        payload: {
            request: {
                url: `/inscripcion/${inscripcion_id}/mensaje`,
                data: obj,
                method: 'POST',
                ...headerConfig(email_estudiante)
            }
        },
    }
}

export function sendMensaje(email_estudiante, conversacion_id, texto){
    return {
        type: ACTION.SEND_MSG,
        payload: {
            request: {
                method: 'POST',
                url: `/perfil/mensajes/${conversacion_id}`,
                data: { texto },
                ...headerConfig(email_estudiante)
            }
        },
    }
}

export function editarPerfil(email_estudiante, estudanteObj){
    return {
        type: ACTION.EDITAR_PERFIL,
        payload: {
            request: {
                method: 'POST',
                url: `/perfil/`,
                data: estudanteObj,
                ...headerConfig(email_estudiante)
            }
        },
    }
}


export function sendFeedback(email_estudiante, feedback){
    return {
        type: ACTION.ENVIAR_FEEDBACK,
        payload: {
            request: {
                method: 'POST',
                url: `/feedback`,
                data: {
                    feedback
                },
                ...headerConfig(email_estudiante)
            }
        },
    }
}