import * as ACTION from './actionTypes';

const baseState = {
    tengo_preguntas: false,
    tengo_hashtags: false,
    tengo_carreras: false,
    tengo_grados: false,
    curso_actual: {},
    cursos: [],
    error: null,
    estudiante_actual: {},
    fontLoaded: true,
    perfil_profesor_actual: {},
    profesor_actual: {},
    profesores: [],
    side_effect: null,
    preguntas: [],
    hashtags: [],
    cursos_aprobados: [],
    distritos: [],
    grados_obtenidos: [],
    carreras_disponibles: [],
}

export default function reducer(state, action) {
    state = state || baseState;

    console.log(action.type);
   
    switch (action.type) {
        case ACTION.FONT_LOADED:
            return {
                ...state,
                fontLoaded: true,
            }
        case ACTION.CLEAR_SIDE_EFFECT:
            return {
                ...state,
                side_effect: null,
            }
        
        case ACTION.SET_LOAD_LOGIN_SE:
            return {
                ...state,
                side_effect: 'LOAD_LOGIN',
            }
                
        case ACTION.SET_CURSO_ACTUAL:
            return {
                ...state,
                curso_actual: action.payload.data,
            }
        case ACTION.SET_PROFESOR_ACTUAL:
            return {
                ...state,
                profesor_actual: action.payload.data,
            }
        case ACTION.SET_LOADING:
            return {
                ...state,
                loading: action.payload.loading,
            }
        
        // Sends the request to axios, put loading ON
        case ACTION.GET_DISTRITOS:
            return {
                ...state,
                loading: !true,
            }
        case ACTION.GET_DISTRITOS_SUCCESS:
            return {
                ...state,
                loading: false,
                distritos: action.payload.data
            }
        case ACTION.GET_DISTRITOS_FAIL:
            return {
                ...state,
                loading: false,
                error: 'Error listando cursos...'
            }
        
        case ACTION.GET_CURSOS:
            return {
                ...state,
                loading: !true,
                cursos: []
            }
        case ACTION.GET_CURSOS_SUCCESS:
            return {
                ...state,
                loading: false,
                cursos: action.payload.data
            }
        case ACTION.GET_CURSOS_FAIL:
            return {
                ...state,
                loading: false,
                error: 'Error listando cursos...'
            }
        
        case ACTION.GET_CURSOS_APROBADOS:
            return {
                ...state,
                loading: !true,
            }
        case ACTION.GET_CURSOS_APROBADOS_SUCCESS:
            return {
                ...state,
                loading: false,
                cursos_aprobados: action.payload.data
            }
        case ACTION.GET_CURSOS_APROBADOS_FAIL:
            return {
                ...state,
                loading: false,
                error: 'Error listando cursos...'
            }

        case ACTION.GET_PROFESORES:
            return {
                ...state,
                loading: !true,
            }
        case ACTION.GET_PROFESORES_SUCCESS:
            return {
                ...state,
                loading: false,
                profesores: action.payload.data
            }
        case ACTION.GET_PROFESORES_FAIL:
            return {
                ...state,
                loading: false,
                error: 'Error listando cursos...'
            }
        case ACTION.LOGIN:
            return {
                ...state,
                loading: !true,
                estudiante_actual: {
estudiante_correo:'estudiante_correo',
estudiante_telefono:'estudiante_telefono',
estudiante_facebook:'estudiante_facebook',
estudiante_distrito:'estudiante_distrito',
estudiante_sexo:'estudiante_sexo',
                },
                side_effect: 'STORE_LOGIN'
            }
        case ACTION.LOGIN_SUCCESS:
            return {
                ...state,
                loading: false,
                estudiante_actual: action.payload.data,
                side_effect: 'STORE_LOGIN'
            }
        case ACTION.SET_CURRENT_LOGIN:
            return {
                ...state,
                loading: false,
                estudiante_actual: action.payload.data,
            }
        case ACTION.LOGIN_FAIL:
            return {
                ...state,
                loading: false,
                side_effect: 'LOGIN_FAILED',
                error: action.payload,
            }

        case ACTION.GET_PERFIL_PROFESOR:
            return {
                ...state,
                loading: !true,
            }
        case ACTION.GET_PERFIL_PROFESOR_SUCCESS:
            return {
                ...state,
                loading: false,
                perfil_profesor_actual: {
                    ...action.payload.data,
                    calificacion: parseFloat(action.payload.data.calificacion),
                    tomar_denuevo: parseFloat(action.payload.data.tomar_denuevo),
                    dificultad: parseFloat(action.payload.data.dificultad),
                },
            }
        case ACTION.GET_PERFIL_PROFESOR_FAIL:
            return {
                ...state,
                loading: false,
                error: action.payload,
            }
        
        case ACTION.GET_PREGUNTAS:
            return {
                ...state,
                tengo_preguntas: false,
                loading: !true,
            }
        case ACTION.GET_PREGUNTAS_SUCCESS:
            return {
                ...state,
                loading: false,
                tengo_preguntas: true,
                preguntas: action.payload.data,

            }
        case ACTION.GET_PREGUNTAS_FAIL:
            return {
                ...state,
                loading: false,
                error: action.payload,
            }
        
        case ACTION.GET_HASHTAGS:
            return {
                ...state,
                tengo_hashtags: false,
                loading: !true,
            }
        case ACTION.GET_HASHTAGS_SUCCESS:
            return {
                ...state,
                loading: false,
                tengo_hashtags: true,
                hashtags: action.payload.data,

            }
        case ACTION.GET_HASHTAGS_FAIL:
            return {
                ...state,
                loading: false,
                error: action.payload,
            }
        case ACTION.GET_GRADOS:
            return {
                ...state,
                tengo_grados: false,
                loading: !true,
            }
        case ACTION.GET_GRADOS_SUCCESS:
            return {
                ...state,
                loading: false,
                tengo_grados: true,
                grados_obtenidos: action.payload.data,
            }

        case ACTION.GET_GRADOS_FAIL:
            return {
                ...state,
                loading: false,
                error: action.payload,
            }
        
        case ACTION.GET_CARRERAS:
            return {
                ...state,
                tengo_carreras: false,
                loading: !true,
            }
        case ACTION.GET_CARRERAS_SUCCESS:
            return {
                ...state,
                loading: false,
                tengo_carreras: true,
                carreras_disponibles: action.payload.data,
            }

        case ACTION.GET_CARRERAS_FAIL:
            return {
                ...state,
                loading: false,
                error: action.payload,
            }
        

        case ACTION.CALIFICAR:
            return {
                ...state,
                loading: !true,
            }
        case ACTION.CALIFICAR_SUCCESS:
            return {
                ...state,
                loading: false,
                side_effect: 'SHOW_SUCCESS',
                perfil_profesor_actual: {
                    ...state.perfil_profesor_actual,
                    puede_calificar: false,
                    puede_comentar: false,
                }
            }
            case ACTION.CALIFICAR_FAIL:
            return {
                ...state,
                loading: false,
                side_effect: 'SHOW_KITTY',
                error: action.payload,
            }
        case ACTION.GET_MENSAJES:
            return {
                ...state,
                loading: !true,
            }
        case ACTION.GET_MENSAJES_SUCCESS:
            return {
                ...state,
                loading: false,
                mensajes: action.payload.data,
            }
            case ACTION.GET_MENSAJES_FAIL:
            return {
                ...state,
                loading: false,
                error: action.payload,
            }
        case ACTION.GET_RANK_CURSO:
            return {
                ...state,
                loading: !true,
            };
        case ACTION.GET_RANK_CURSO_SUCCESS:
            return {
                ...state,
                loading: false,
                ranking: action.payload.data,
            };
        case ACTION.GET_RANK_CURSO_FAIL:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        case ACTION.GET_RANK_RECOMENDADO:
            return {
                ...state,
                loading: !true,
            };
        case ACTION.GET_RANK_RECOMENDADO_SUCCESS:
            return {
                ...state,
                loading: false,
                ranking: action.payload.data,
            };
        case ACTION.GET_RANK_RECOMENDADO_FAIL:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        case ACTION.GET_RANK_VOTADO:
            return {
                ...state,
                loading: !true,
            };
        case ACTION.GET_RANK_VOTADO_SUCCESS:
            return {
                ...state,
                loading: false,
                ranking: action.payload.data,
            };
        case ACTION.GET_RANK_VOTADO_FAIL:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        case ACTION.SET_COMENTARIO_ACTUAL:
            return {
                ...state,
                comentario_actual: action.payload.data,
            }
        
        case ACTION.SET_CHAT_ACTUAL: 
            return {
                ...state,
                chat_actual: action.payload.data,
            };

        case ACTION.START_CHAT:
            return {
                ...state,
                loading: !true,
            };
        
        case ACTION.START_CHAT_SUCCESS:
            return {
                ...state,
                loading: false,
                chat_actual: action.payload.data,
                side_effect: 'REDIRECT_CHAT'
            };
        case ACTION.START_CHAT_FAIL:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        case ACTION.SEND_MSG:
            return {
                ...state,
                loading: !true,
            };
        
        case ACTION.SEND_MSG_SUCCESS:
            return {
                ...state,
                loading: false,
                conversacion: state.conversacion.concat([action.payload.data]),
            };
        case ACTION.SEND_MSG_FAIL:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        
        case ACTION.GET_COMMENTS:
            return {
                ...state,
                loading: !true,
            };
        case ACTION.GET_COMMENTS_SUCCESS:
            return {
                ...state,
                loading: false,
                comentarios: action.payload.data,
            };
        case ACTION.GET_COMMENTS_FAIL:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        
        case ACTION.GET_CHAT:
            return {
                ...state,
                loading: !true,
            };
        case ACTION.GET_PERFIL_ESTUDIANTE:
            return {
                ...state,
                loading: !true,
            };
        case ACTION.GET_PERFIL_ESTUDIANTE_SUCCESS:
            return {
                ...state,
                estudiante_actual: action.payload.data,
                loading: false,
            };
        case ACTION.GET_PERFIL_ESTUDIANTE_FAIL:
            return {
                ...state,
                error: action.payload,
                loading: false,
            };
        
        case ACTION.EDITAR_PERFIL:
            return {
                ...state,
                loading: !true,
            };
        case ACTION.EDITAR_PERFIL_SUCCESS:
            return {
                ...state,
                estudiante_actual: action.payload.data,
                loading: false,
            };
        case ACTION.EDITAR_PERFIL_FAIL:
            return {
                ...state,
                error: action.payload,
                loading: false,
            };
        case ACTION.GET_CHAT_SUCCESS:
            return {
                ...state,
                loading: false,
                conversacion: action.payload.data,
            };
        case ACTION.GET_CHAT_FAIL:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
    
        case ACTION.CREATE_NEW_CHAT:
            return {
                ...state,
                loading: !true,
            };
        case ACTION.CREATE_NEW_CHAT_SUCCESS:
            return {
                ...state,
                loading: false,
                chat_actual: action.payload.data,
                side_effect: 'START_CHAT'
            };
        case ACTION.CREATE_NEW_CHAT_FAIL:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
    
        case ACTION.ENVIAR_FEEDBACK:
            return {
                ...state,
                loading: !true,
            };
        case ACTION.ENVIAR_FEEDBACK_SUCCESS:
            return {
                ...state,
                loading: false,
                chat_actual: action.payload.data,
                side_effect: 'RETURN_PROFILE'
            };
        case ACTION.ENVIAR_FEEDBACK_FAIL:
            console.log(action);
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        default:
            return state;
    }
}

