import {Map} from 'immutable'
import {EventEmitter} from 'eventemitter3'
import {get as getKey} from 'lodash'

export default class Store extends EventEmitter {
    currentState;

    constructor(initialState){
        super();
        this.currentState = Map(initialState)
    }

    mutate(fn) {
        this.currentState = this.currentState.merge(fn(this.currentState) || {});
        this.emit('change');
    }

    get(keypath, def = null){
        return this.currentState.getIn(keypath.split('.'), def);
    }
}