import Color from 'color';
import { StatusBar, StyleSheet, Dimensions, Platform} from 'react-native';
import * as constants from './constants';

const DEVICE_SCALE = Dimensions.get("window").width / 375;

export function normalize(size) {
    return Math.round(DEVICE_SCALE * size);
}

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;

var whiteBg = {
    backgroundColor: '#fff',
}

var fullHeight = {
    height: '100%'
}

var fullWidth = {
    width: '100%',
}

var topBarPadding = {
    paddingTop: StatusBar.currentHeight,
}

var fontSizes = {
    h1: {
        fontSize: normalize(32),
    },
    h2: {
        fontSize: normalize(28),
    },
    h3: {
        fontSize: normalize(24),
    },
    h4: {
        fontSize: normalize(20),
    },
    h5: {
        fontSize: normalize(16),
    },
    small: {
        fontSize: normalize(11),
    },
}

var backButtonPadding = {
    paddingTop: 80,
}

export default {
    BaseText: {
        color: constants.COLOR_TEXT,
    },
    TextH1: {
        ...fontSizes.h1,
    },
    TextH2: {
        ...fontSizes.h2,
    },
    TextH3: {
        ...fontSizes.h3,
    },
    TextH4: {
        ...fontSizes.h4,
    },
    TextH5: {
        ...fontSizes.h5,
    },
    TextSmall: {
        ...fontSizes.small,
    },

    LoginView: {
        backgroundColor: '#4cb0f4', 
        ...fullHeight, 
        // ...topBarPadding,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    LogoWrapper: {
        position: 'absolute',
        top: 75,
        height: 150,
        right: 0,
    },
    LoginGradient: { 
        justifyContent: 'flex-end', 
        flex: 1, 
        width: '100%'
    },
    LoginText: {
        color: 'white',
        fontSize: 32,
        fontWeight: 'bold',
        marginBottom: 15,
        marginLeft: 15,
    },
    LoginButton: {
        backgroundColor: '#8526FF',
        borderRadius: 14,
        marginVertical: 5,
        paddingVertical: 3,
        marginHorizontal: 40,
        marginTop: 20,
    },
    StartFeaturedText: {
        ...fontSizes.h1,
        ...Platform.select({
            android:{
                lineHeight: 40,
                fontWeight: '400',
            },
        }),
        paddingHorizontal: 12,
        textAlign: 'left',
        color: 'white',
        marginBottom: 20,
        fontFamily: 'avenir-black',
        lineHeight: normalize(35),
    },
    StartMainView: {
        ...whiteBg,
        flex: 1,
    },
    BackButtonPadding: {
        ...backButtonPadding
    },
    StartMainGradient: {
        ...backButtonPadding,
        // paddingBottom: 20,
        justifyContent: 'flex-end',
        alignItems: 'flex-start',
        width: '100%',
        marginBottom: 16,
    },
    StartMainGridItem: {
        backgroundColor: 'white',
        borderRadius: 8,
        // marginBottom: 4,
        alignItems: 'center',
        justifyContent: 'center',
        height: 96,
        ...Platform.select({
            android: {
                elevation: 4,
            },
            ios: {
                shadowColor: 'rgba(164,164,164, 0.35)',
                shadowOpacity: 1,
                shadowOffset: {
                    width: 0,
                    height: 5,
                },
                shadowRadius: 5,
            }
        }),
    }
}