import React from 'react';

import {Icon} from 'react-native-elements';

import Color from 'color';

export const COLOR_PRIMARY = '#53bae9';
export const COLOR_SECONDARY = '#8526ff';
export const COLOR_TERTIARY = '#4CB0F4';
export const COLOR_TEXT = '#2C2C2C';
export const COLOR_DISABLED = '#AAAAAA';
export const COLOR_ERROR = '#FF3232';
export const COLOR_CHEVRON = '#61f4d6';


var primary = "#c8001e";

export const bgColor = Color('#fff').string();
export const primaryBgColor = Color(primary).string();
export const primaryDarkBgColor = Color(primary).darken(0.25).string();
export const primaryDarkerBgColor = Color(primaryDarkBgColor).darken(0.35).string();
export const primaryLightBgColor = Color(primary).lighten(1.1).string();

export const primaryLighterBgColor = Color(primary).lighten(0.55).string();

export function tabIcon(iconProps){
    iconProps = Object.assign({
        type: "foundation",
        name: 'torso',
        size: 28,
    }, iconProps);
    return ({focused}) => {
        var color = "#666";
        if(focused){
            color = "#ccc";
        }
        return (<Icon {...iconProps} color={color} />)
    }
}

// ---

export const ATOMO_LOGIN = require('./assets/icons/atomo-login.png')
export const ATOMO_LOGIN_PRIMARY = require('./assets/icons/upc-aula.png')
export const DEFAULT_ICON_CURSO = require('./assets/icons/book-color.png')
export const DEFAULT_ICON_RESENIA_PROFE = require('./assets/icons/bio-teacher.png')
export const DEFAULT_ICON_RESENIA_EST = require('./assets/icons/bio-student.png')

export const DEFAULT_ICON_ALUMNO = require('./assets/icons/perfil-alumno-default.png')
export const DEFAULT_ICON_ALUMNA = require('./assets/icons/perfil-alumna-default.png')

export const ICON_BACK = require('./assets/icons/back-icon.png')
export const DEFAULT_ICON_PROFESOR = require('./assets/icons/perfil-profesor-default.png')
export const DEFAULT_ICON_PROFESORA = require('./assets/icons/perfil-profesora-default.png')
export const ICON_STAR = require('./assets/icons/star-empty.png')

export const ATOMO_LISTA_PROFESOR = require('./assets/icons/atomo-lista-profesor.png')
export const ATOMO_BG = require('./assets/icons/atomo-bg.png')
export const ATOMO_BG_BIO = require('./assets/icons/student-bio-bg.png')


export const ICONO_BOOK_COLOR = require('./assets/icons/book-color.png');
export const ICONO_BOOK_OFF = require('./assets/icons/book-off.png');

export const ICONO_MEDALLA_COLOR = require('./assets/icons/medalla-color.png');
export const ICONO_MEDALLA_OFF = require('./assets/icons/medalla-off.png');

export const ICONO_BIRRETE_SPLASH = require('./assets/icons/birrete-splash.png');

export const ICONO_BIRRETE_COLOR = require('./assets/icons/birrete-color.png');
export const ICONO_BIRRETE_OFF = require('./assets/icons/birrete-off.png');

export const ICONO_DIPLOMA_COLOR = require('./assets/icons/diploma-color.png');
export const ICONO_DIPLOMA_OFF = require('./assets/icons/diploma-off.png');

export const ICONO_COPA_COLOR = require('./assets/icons/cup-color.png');
export const ICONO_FLECHA_CHAT = require('./assets/icons/chat-arrow-color.png');

export const ICONO_FLECHA_FEEDBACK = require('./assets/icons/feedback-arrow.png');

export const ICONO_PI = require('./assets/icons/matematica.png');
export const ICONO_PANTEON = require('./assets/icons/univsersity-color.png');
export const ICONO_MEDALLA_COLOR_GRANDE = require('./assets/icons/medalla-grande-color.png');
export const ICONO_MEDALLA_OFF_GRANDE = require('./assets/icons/medalla-grande-off.png');
export const ICONO_GLOBO_COLOR = require('./assets/icons/comment-color.png');
export const IMG_GATO = require('./assets/icons/gato.png');

export const IMG_SLIDE_1 = require('./assets/img/slide-1.png');
export const IMG_SLIDE_2 = require('./assets/img/slide-2.png');
export const IMG_SLIDE_3 = require('./assets/img/slide-3.png');

export const IMG_GATOS_REPETIDOS = require('./assets/img/gato-2.png');

export const IMG_LOGO_UPC = require('./assets/icons/upc-logo.png');
