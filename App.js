import React from 'react';
import { Animated, Image, Text, Platform, StatusBar, StyleSheet, View } from 'react-native';
import { AppLoading, Icon, SplashScreen } from 'expo';
import { Asset } from 'expo-asset';
import * as Font from 'expo-font';
import AppNavigator from './AppNavigator';
import * as constants from './constants'
import { Image as RNA_Image} from 'react-native-animatable'


export default class App extends React.Component {
  state = {
    isLogoComplete: false,
    isLoadingComplete: false,
    splashAnimation: new Animated.Value(0),
    splashAnimationComplete: false,
  }
  componentDidMount() {
    SplashScreen.preventAutoHide();
    this._loadAsync();
  }

  handleImgRef = ref => this.img = ref;

  delay = (timeout) => {
    return new Promise((resolve) =>{
        setTimeout(function(){ resolve() }, timeout)
      })
  }

  _loadAsync = async () => {
    try {
      console.log('starting to load')
      await this._loadFirstImageAsync();
      SplashScreen.hide();
      console.log('logo finished')
      this.setState({
        isLogoComplete: true,
      })
      console.log('fetchin the rest')
      await this._loadResourcesAsync();
    } catch (e) {
      this._handleLoadingError(e);
    } finally {
      /*
      Update dis
      this.img.transitionTo({
        opacity: 0,
        rotate: '180deg',
        scale: 4,
      }, 1000, 'ease', 200);
      await this.delay(10);
      */
      this._handleFinishLoading();
    }
  };
  
  render() {
    if (!this.state.isLogoComplete) {
      console.log('rendering empty view')
    }
    
    if (!this.state.isLoadingComplete) {
      console.log('rendering logo')
      return <View style={[styles.container, {
        // flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
      }]}>
        <RNA_Image
          ref={this.handleImgRef}
          useNativeDriver
          animation="pulse"
          iterationCount="infinite"
          source={require('./assets/splash.png')}
          fadeDuration={150}
          resizeMode="contain"
          style={{
            width: undefined,
            height: undefined,
            position: 'absolute',
            top: 0,
            left: 0,
            bottom: 0,
            right: 0,
          }}
          />
      </View>
    }
    
    console.log('rendering the rest')
    return (
      <View style={styles.container}>
        {Platform.OS === 'ios' && <StatusBar barStyle="light-content" />}
        <AppNavigator /> 
        {/* {this._maybeRenderFadeSplash()} */}
      </View>
    );
  }

  _maybeRenderFadeSplash = () => {
    if (this.state.splashAnimationComplete) {
      return null;
    }

    return (
      <Animated.View
        style={{
          position: 'absolute',
          top: 0,
          left: 0,
          bottom: 0,
          right: 0,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: constants.COLOR_PRIMARY,
          opacity: this.state.splashAnimation.interpolate({
            inputRange: [0, 1],
            outputRange: [1, 0],
          }),
        }}>
        <Animated.Image
          source={require('./assets/splash.png')}
          fadeDuration={150}
          resizeMode="contain"
          style={{
            width: undefined,
            height: undefined,
            position: 'absolute',
            top: 0,
            left: 0,
            bottom: 0,
            right: 0,
            transform: [
              {
                rotate: this.state.splashAnimation.interpolate({
                  inputRange: [0, 1],
                  outputRange: ['0deg', '180deg'],
                }),
              },
              {
                scale: this.state.splashAnimation.interpolate({
                  inputRange: [0, 1],
                  outputRange: [1, 4],
                }),
              },
            ],
          }}
          onLoadEnd={this._animateOut}
        />
      </Animated.View>
    );
  };

  _animateOut = () => {
    
    Animated.timing(this.state.splashAnimation, {
      delay: 300,
      toValue: 1,
      duration: 500,
      useNativeDriver: true,
    }).start(() => {
      this.setState({ splashAnimationComplete: true });
    })
  };

  _loadFirstImageAsync = async () => {
    return Asset.loadAsync(require('./assets/splash.png'));
  }

  _loadResourcesAsync = async () => {
    return Promise.all([
      Asset.loadAsync([
        constants.ICON_STAR,
        constants.DEFAULT_ICON_CURSO,
        constants.DEFAULT_ICON_PROFESOR,
        constants.ATOMO_LISTA_PROFESOR,
        constants.ATOMO_BG,
        constants.ICONO_MEDALLA_OFF_GRANDE,
        constants.ICONO_COPA_COLOR,
        constants.IMG_SLIDE_1,
        constants.IMG_SLIDE_2,
        constants.IMG_SLIDE_3,
        constants.IMG_LOGO_UPC,
      ]),
      Font.loadAsync({
        'avenir-book': require('./assets/fonts/AvenirLTStd-Book.otf'),
        'avenir-black': require('./assets/fonts/AvenirLTStd-Black.otf'),
      }),
    ]);
  };

  _handleLoadingError = error => {
    // In this case, you might want to report the error to your error
    // reporting service, for example Sentry
    console.warn(error);
  };

  _handleFinishLoading = () => {
    this.setState({ isLoadingComplete: true });
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: constants.COLOR_PRIMARY,
  },
});
