
const mockProfesores = [
    {
        profesorId: 1,
        nombre: 'LOREM IPSUM DOLOR',
        bio: 'le di clase a amstrong y a einstein!',
        sexo: 'M',
    },
    {
        profesorId: 10,
        nombre: 'OTRO PROFE',
        bio: 'le di clase a amstrong y a einstein!',
        sexo: 'F',
    },
    {
        profesorId: 12,
        nombre: 'MAS OTRO PROFE',
        bio: 'le di clase a amstrong y a einstein!',
        sexo: 'F',
    },
    {
        profesorId: 2,
        nombre: 'LOREM 222 DOLOR',
        bio: 'le di clase a amstrong y a einstein!',
        sexo: 'M',
    },
    {
        profesorId: 3,
        nombre: '3333 IPSUM DOLOR',
        bio: 'le di clase a amstrong y a einstein!',
        sexo: 'F'
    },
    {
        profesorId: 4,
        nombre: 'Sarah Connor',
        bio: 'le di clase a amstrong y a einstein!',
        sexo: 'M'
    },
    {
        profesorId: 6,
        nombre: 'John Connor',
        bio: 'le di clase a amstrong y a einstein!',
        sexo: 'M'
    },
    {
        profesorId: 5,
        nombre: 'Sarah Connor',
        bio: 'le di clase a amstrong y a einstein!',
        sexo: 'F'
    },
];

import axios from 'axios';

const $http = axios.create({
    baseURL: 'http://upc.josegomezr.com.ve/'
})
class WebAPI {

    apiToken = '387730c4-201c-330f-a12c-a38085324c5c';
    email = 'usuario@upc.edu.pe';

    keepEmail(email){
        this.email = email;
    }
    
    clearEmail(){
        this.email = null;
    }

    buildHeaders(){
        let resp = {
            "Api-Token": this.apiToken,
            "User-Agent": 'AulaUPC Mobile',
        }
        if (this.email){
            resp["Intranet-Email"] = this.email;
        }
        // resp["Intranet-Email"] = 'usuario@upc.edu.pe';
        return resp;
    }

    async getCursos(periodo=null){
        try{
            var response = await $http.get(
                `/cursos`,
                {
                    headers: this.buildHeaders(),
                }
            )
            return Promise.resolve(response.data);
        }catch(e){
            // console.error(e);
            return Promise.reject(e);
        }
    }
    
    async getProfesoresByCurso(cursoId=null){
        try{
            var response = await $http.get(
                `/cursos/${cursoId}/profesores`,
                {
                    headers: this.buildHeaders(),
                }
            )
            return Promise.resolve(response.data);
        }catch(e){
            // onsole.error(e);
            return Promise.reject(e);
        }
    }
    
    async getProfesorByCurso(cursoId=null, profesorId=null){
        try{
            var response = await $http.get(
                `/cursos/${cursoId}/profesores/${profesorId}`,
                {
                    headers: this.buildHeaders(),
                }
            )
            return Promise.resolve(response.data);
        }catch(e){
            // onsole.error(e);
            return Promise.reject(e);
        }
    }

    async getComentariosByCursoProfesor(cursoId, profesorId){
        try{
            var response = await $http.get(
                `/cursos/${cursoId}/profesores/${profesorId}/comentarios`,
                {
                    headers: this.buildHeaders(),
                }
            )
            return Promise.resolve(response.data);
        }catch(e){
            // onsole.error(e);
            return Promise.reject(e);
        }
    }

    async sendComentario(inscripcionId, texto){
        try{
            var response = await $http.post(
                `/inscripcion/${inscripcionId}/comentar`,
                {
                    texto
                },
                {
                    headers: this.buildHeaders(),
                }
            )
            return Promise.resolve(response.data);
        }catch(e){
            // onsole.error(e);
            return Promise.reject(e);
        }
    }
    
    async iniciarHilo(inscripcionId, asunto, texto){
        try{
            var response = await $http.post(
                `/inscripcion/${inscripcionId}/mensaje`,
                {
                    asunto, texto
                },
                {
                    headers: this.buildHeaders(),
                }
            )
            return Promise.resolve(response.data);
        }catch(e){
            // onsole.error(e);
            return Promise.reject(e);
        }
    }

    async getRanking(){
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(
                    mockProfesores.map((item) => {
                        item.calificacion = ((Math.random()*500)|0)/100;
                        return item;
                    }).sort((a, b) => a.calificacion > b.calificacion)
                )
            }, 600)
        })
    }

    async tryLogin(email, password){
        try{
            var response = await $http.post('/login', {
                email, password
            }, {
                headers: this.buildHeaders(),
            })
            return Promise.resolve(response.data);
        }catch(e){
            return Promise.reject(e);
        }
    }
    
    async getPerfil(){
        try{
            var response = await $http.get('/perfil', {
                headers: this.buildHeaders(),
            })
            return Promise.resolve(response.data);
        }catch(e){
            return Promise.reject(e);
        }
    }
    
    async getConversaciones(){
        try{
            var response = await $http.get('/perfil/mensajes', {
                headers: this.buildHeaders(),
            })
            return Promise.resolve(response.data);
        }catch(e){
            return Promise.reject(e);
        }
    }
    
    async getMensajes(hiloId){
        try{
            var response = await $http.get(`/perfil/mensajes/${hiloId}`, {
                headers: this.buildHeaders(),
            })
            return Promise.resolve(response.data.mensajes);
        }catch(e){
            return Promise.reject(e);
        }
    }
    
    async enviarMensaje(hiloId, texto){
        try{
            var response = await $http.post(`/perfil/mensajes/${hiloId}`, {
                texto,
            },{
                headers: this.buildHeaders(),
            })
            return Promise.resolve(response.data.mensajes);
        }catch(e){
            return Promise.reject(e);
        }
    }
    
    async getPreguntas(){
        try{
            var response = await $http.get(`/preguntas`, {
                headers: this.buildHeaders(),
            })
            return Promise.resolve(response.data);
        }catch(e){
            return Promise.reject(e);
        }
    }

    async getHashtags(){
        try{
            var response = await $http.get(`/hashtags`, {
                headers: this.buildHeaders(),
            })
            return Promise.resolve(response.data);
        }catch(e){
            return Promise.reject(e);
        }
    }

    async sendCalificaciones( inscripcionId, respuestas, hashtags_seleccionados, dificultad, tomar_denuevo){
        try{
            var response = await $http.post(
                `/inscripcion/${inscripcionId}/calificar`, {
                    respuestas,
                    hashtags_seleccionados,
                    dificultad,
                    tomar_denuevo
                },
                {
                headers: this.buildHeaders(),
            })
            return Promise.resolve(response.data);
        }catch(e){
            return Promise.reject(e);
        }
    }
}
export default new WebAPI();