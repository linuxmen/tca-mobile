import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Alert,
    Dimensions,
} from 'react-native';

import * as constants from 'aula-upc/constants'
import {Icon, Input, Button} from 'react-native-elements';

const SCREEN_WIDTH = Dimensions.get('window').width;

class Sugerencias extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            asunto: '',
            mensaje: '',
        }
    }

    mensaje;

    static navigationOptions = {
        title: 'Sugerencias',
        headerStyle: {
            backgroundColor: constants.primaryBgColor
        },
        headerTitleStyle: {
            color: constants.bgColor,
        },        
    }
    
    _onPress(){
        Alert.alert('Tus sugerencias han sido enviadas correctamente!')
        this.setState({
            asunto: '',
            mensaje: '',
        });
    }

    onChangeSubject(){
        if(this.mensaje){
            this.mensaje.focus();
        }
    }

    render() {
        return (
            <View style={styles.mainView}>
                <Input underlineColorAndroid="transparent" 
                    keyboardAppearance="light"
                    placeholder="Sugerencia de Información"
                    onChangeText={(text) => { this.setState({'asunto': text})} }
                    label="Asunto"
                    containerStyle={{ alignSelf: 'center'}}
                    labelStyle={{
                        color: constants.primaryBgColor,
                    }}
                    returnKeyType="next"
                    blurOnSubmit={true}
                    value={this.state.asunto}
                    onSubmitEditing={() => { this.onChangeSubject() }}
                    inputContainerStyle={{
                        borderBottomColor: 'transparent',
                        borderTopColor: constants.primaryBgColor,
                        borderTopWidth: 1
                    }} 
                    />
                <Input underlineColorAndroid="transparent" 
                    ref={(input) => (this.mensaje = input)}
                    onChangeText={(text) => { this.setState({'mensaje': text})} }
                    value={this.state.mensaje}
                    multiline={true}
                    numberOfLines={6}
                    // keyboardAppearance="light"
                    placeholder="Escribe tus sugerencias aqui..."
                    inputStyle={{
                        textAlignVertical: 'top',
                        height: 120,
                    }}
                    label="Mensaje"
                    containerStyle={{
                        alignSelf: 'center',
                    }}
                    labelStyle={{
                        color: constants.primaryBgColor,
                    }}
                    inputContainerStyle={{
                        borderBottomWidth: 0,
                        borderTopColor: constants.primaryBgColor,
                        borderTopWidth: 1
                    }} 
                    />
                
                <View style={{width: 100, alignSelf: 'center', }}>
                    <Button 
                        title="Enviar" 
                        onPress={() => this._onPress()} 
                        buttonStyle={{
                            backgroundColor: constants.primaryBgColor
                        }}
                        />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    mainView: {
        backgroundColor: constants.bgColor,
        paddingTop: 4,
        height: '100%',
        width: '100%',
    },
});

import { createStackNavigator } from 'react-navigation';


var sugerenciasStack = createStackNavigator({
    sugerenciasMain: { screen: Sugerencias }
})


sugerenciasStack.navigationOptions = {
    title: 'Sugerencias',
    tabBarIcon: constants.tabIcon({
        type: 'entypo',
        name: 'mail'
    }) 
}
export default sugerenciasStack