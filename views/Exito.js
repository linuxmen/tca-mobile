import React from 'react';
import {
    StyleSheet,
    FlatList,
    View,
    Button,
    Alert,
    Image,
    TouchableHighlight,
} from 'react-native';

import Text from '../components/BaseText'
import Loading from 'aula-upc/components/Loading'

import * as constants from '../constants'
import Style from '../stylesheet'

class Exito extends React.Component {
    componentDidMount(){
        this.props.clearSideEffects();
    }

    render() {
        return (
            <View style={styles.mainView}>
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    flex: 1,
                }}>
                    <View style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                        <Image source={constants.ICONO_PANTEON}
                            resizeMode="contain"
                            style={{
                                width: 48
                            }}
                        />
                    </View>

                    <View style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                        <Image source={constants.ICONO_MEDALLA_COLOR_GRANDE}
                            resizeMode="contain"
                            style={{
                                width: 120
                            }}
                        />
                    </View>
                </View>
                <View style={{
                }}>
                    <Text>Registro Correcto</Text>
                </View>
                <View style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>
                    <Image source={constants.ICONO_PI}
                        resizeMode="contain"
                        style={{
                            width: 96
                        }}
                    />
                </View>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    mainView: {
        ...Style.StartMainView,
        alignItems: 'center',
        justifyContent: 'center',
    },
});

import { connect } from 'react-redux'

import { clearSideEffects } from '../redux/actions';

const mapStateToProps = state => {
    return {
    };
};

const mapDispatchToProps = {
    clearSideEffects
};


export default connect(mapStateToProps, mapDispatchToProps)(Exito);
