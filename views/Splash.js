import React, { Component } from "react";
import {
  Dimensions,
  ScrollView,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
} from "react-native";

import { LinearGradient } from 'expo'

import * as constants from '../constants';

import { normalize } from '../stylesheet'

const SCREEN_WIDTH = Dimensions.get("window").width;
const SCREEN_HEIGHT = Dimensions.get("window").height;

const Screen = props => {

  const Component = props.gradient ? LinearGradient : View
  var componentProps = {
    style: [styles.screen,]
  }
  if (!props.gradient) {
    componentProps.style.push({
      backgroundColor: props.backgroundColor,
    })
  }else{
    componentProps.colors = props.colors
  }

  return (
    <View style={styles.scrollPage}>
      <Component {...componentProps} >
        {props.index == 0 && (
          <Image
            fadeDuration={0}
            source={constants.ATOMO_LOGIN}
            resizeMode="stretch"
            style={{
              position: 'absolute',
              right: 0,
              height: 300,
              width: 140,
              top: 190,
            }}
          />
        )}

        {props.index == 1 && (
          <Image
            fadeDuration={0}
          source={constants.ATOMO_LISTA_PROFESOR}
          resizeMode="stretch"
          style={{
              position: 'absolute',
              right: 16,
              height: 108,
              width: 96,
              top: 56,
            }}
            />
        )}
        {props.index == 1 && (
          <Image
            fadeDuration={0}
            source={constants.ICONO_BIRRETE_SPLASH}
            resizeMode="stretch"
            style={{
              position: 'absolute',
              height: 48,
              width: 96,
              top: 180,
            }}
          />
        )}
        <Text style={styles.slideMainText}>{props.text}</Text>
        <View style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          marginVertical: 16,
        }}>
          <View style={[styles.slideIndicator]}></View>
          <View style={[styles.slideIndicator, {
            opacity: props.index < 1 ? 0.6 : 1
          }]}></View>
          <View style={[styles.slideIndicator, {
            opacity: props.index < 2 ? 0.6 : 1
          }]}></View>
        </View>
      </Component>
      <TouchableOpacity style={{
        position: 'absolute',
        bottom: 0,
        width: '100%',
        paddingHorizontal: 16,
        justifyContent: 'center',
        alignItems: 'center',
      }} activeOpacity={0.8} onPress={() => Actions.replace('login')}>
        <Image
          style={{
            height: SCREEN_HEIGHT * 0.57,
            width: SCREEN_WIDTH*0.71,
          }}
          source={props.image}
          fadeDuration={0}
          resizeMode="stretch"
        />
      </TouchableOpacity>
    </View>
  );
};


export class Splash extends Component {
  render() {
    return (
      <ScrollView
        scrollEventThrottle={16}
        horizontal
        pagingEnabled
        style={styles.scrollView}
        showsHorizontalScrollIndicator={false}
      >
        <Screen
          text="Encuentra el mejor profesor para tus cursos"
          backgroundColor={constants.COLOR_PRIMARY}
          index={0}
          image={constants.IMG_SLIDE_1}
        />
        <Screen 
          text="Califica y comenta sobre el nivel de tu profesor" 
          backgroundColor={constants.COLOR_SECONDARY}
          index={1} 
          image={constants.IMG_SLIDE_2}
        />
        <Screen 
          gradient={true}
          text=" Pregunta a la comunidad sobre tu profesor" 
          colors={[constants.COLOR_TERTIARY, constants.COLOR_SECONDARY]}
          index={2} 
          image={constants.IMG_SLIDE_3}
        />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  scrollView: {
    flexDirection: "row",
  },
  scrollPage: {
    width: SCREEN_WIDTH,
    flex:1,
  },
  screen: {
    width: SCREEN_WIDTH,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 16,
    height: '70%', 
    paddingBottom: 30,
  },
  text: {
    fontSize: 45,
    fontWeight: "bold"
  },
  slideIndicator: {
    width: 12,
    height: 12,
    backgroundColor: 'white',
    marginHorizontal: 8,
    borderRadius: 12,
    overflow: 'hidden',
  },
  slideMainText: {
    color: 'white',
    textAlign: 'center',
    fontFamily: 'avenir-black',
    fontSize: normalize(23),
    lineHeight: normalize(28),
  },
});

import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

export default connect()(Splash);
