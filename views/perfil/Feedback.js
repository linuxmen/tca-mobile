import React from 'react';

import {
    Platform,
    StatusBar,
    StyleSheet,
    FlatList,
    View,
    Button,
    Alert,
    Image,
    TouchableHighlight,
    KeyboardAvoidingView
} from 'react-native';

import { LinearGradient } from 'expo'

import CursoItem from '../../components/CursoItem'
import Text from '../../components/BaseText'
import Input from '../../components/BaseInput'
import Loading from 'aula-upc/components/Loading'

import * as constants from '../../constants'
import Style from '../../stylesheet'
import {Button as RNEButton} from 'react-native-elements'

import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

import GradientButton from '../../components/GradientButton'

import BaseViewWithBackButton from '../../components/BaseViewWithBackButton'

class Start extends BaseViewWithBackButton {
    state = {
        feedback: ''
    }

    async componentDidUpdate() {
        if (!this.props.side_effect) {
            return;
        }

        if (this.props.side_effect == 'RETURN_PROFILE') {
            this.props.clearSideEffects();
            Alert.alert('Información', 'Tu sugerencia se ha enviado exitosamente', [
                {
                    text: 'OK', onPress: () => {
                        this.setState({
                            feedback: '',
                        });
                        Actions.pop();
                    }
                },
            ],
            { cancelable: false }
            )
            return;
        }

    }
    render() {
        if (this.props.loading) {
            return (<Loading message="Enviando datos..."/>)
        }

        return (
            <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={5} enabled style={styles.mainView}>
                <StatusBar barStyle="light-content" backgroundColor={constants.COLOR_SECONDARY} />
                <LinearGradient style={styles.gradientStyle}
                    colors={[constants.COLOR_TERTIARY, constants.COLOR_SECONDARY]}
                    >
                    <Image
                        source={constants.ICONO_MEDALLA_OFF_GRANDE}
                        resizeMode="stretch"
                        style={{
                            width: 80,
                            height: 96,
                            opacity: 1,
                            position: 'absolute',
                            right: 16,
                            top: 24,
                            ...Platform.select({
                                android: {
                                    top: 32,
                                }
                            })
                        }} />
                    <Text style={styles.featuredText}>Feedback</Text>
                </LinearGradient>

                <View style={styles.formWrapper}>
                    <Input underlineColorAndroid="transparent"
                        placeholder="Ingresa aquí tu sugerencia..."
                        inputStyle={styles.inputStyle}
                        containerStyle={{ alignSelf: 'center' }}
                        inputContainerStyle={styles.inputContainerStyle}
                        autoCapitalize="none"
                        keyboardAppearance="light"
                        keyboardType="default"
                        autoCorrect={false}
                        returnKeyType="next"
                        blurOnSubmit={true}
                        multiline={true}
                        numberOfLines={4}
                        onChangeText={(text) => { this.setState({ 'feedback': text }) }}
                        value={this.state.feedback}
                    />
                    <GradientButton
                        text="Enviar"
                        textProps={{
                            h5: true,
                        }}
                        style={{
                            marginHorizontal: 32,
                            paddingHorizontal: 4,
                            paddingVertical: 10,
                            borderRadius: 8,
                            overflow: 'hidden'
                        }}
                        textStyle={{
                            color: 'white',
                            textAlign: 'center',
                        }}
                        onPress={() => {
                            this.props.sendFeedback(this.props.estudiante_actual.estudiante_correo)
                        }}
                        colors={[constants.COLOR_TERTIARY, constants.COLOR_SECONDARY]}
                        start={{ x: 0, y: 0 }}
                        end={{ x: 1, y: 0 }}
                    />
                </View>
            </KeyboardAvoidingView>
        );
    }

}


const styles = StyleSheet.create({
    mainView: {
        ...Style.StartMainView
    },
    gradientStyle: {
        ...Style.StartMainGradient
    },
    featuredText:{
        ...Style.StartFeaturedText,
    },
    inputContainerStyle: {
        marginVertical: 3,
        borderRadius: 10,
        backgroundColor: "transparent",
        paddingVertical: 10,
        // justifyContent: 'flex-start',
        borderBottomWidth: 0,
    },
    inputStyle: {
        height: 200,
        // paddingRight: 20,
        backgroundColor: 'white',
        // fontSize: 18,
    },
    loginBtn: {
        ...Style.LoginButton,
    },
    formWrapper: {
        width: '100%',
    },
});


import { sendFeedback, clearSideEffects } from '../../redux/actions';

const mapStateToProps = state => {
    return {
        estudiante_actual: state.estudiante_actual || {},
        loading: state.loading,
        side_effect: state.side_effect,
    };
};

const mapDispatchToProps = {
    sendFeedback,
    clearSideEffects
};


export default connect(mapStateToProps, mapDispatchToProps)( Start );
