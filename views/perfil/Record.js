import React from 'react';
import {
    Platform,
    StatusBar,
    StyleSheet,
    FlatList,
    View,
    Button,
    Alert,
    Image,
    TouchableHighlight,
} from 'react-native';

import { LinearGradient } from 'expo'

import CursoAprobado from '../../components/CursoAprobado'
import Text from '../../components/BaseText'
import Loading from 'aula-upc/components/Loading'

import * as constants from '../../constants'
import Style from '../../stylesheet'
import {Button as RNEButton} from 'react-native-elements'

import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';


import BaseViewWithBackButton from '../../components/BaseViewWithBackButton'

class Record extends React.Component {
    componentDidMount(){
        this.props.listCursosAprobados(this.props.estudiante_actual.estudiante_correo);
    }

    render() {
        if (this.props.loading) {
            return (<Loading message="Cargando record académico..."/>)
        }

        return (
            <View style={styles.mainView}>
                <StatusBar barStyle="light-content" backgroundColor={constants.COLOR_SECONDARY} />
                <LinearGradient style={styles.gradientStyle}
                    colors={[constants.COLOR_TERTIARY, constants.COLOR_SECONDARY]}
                    >
                    <Image
                        source={constants.ICONO_MEDALLA_OFF_GRANDE}
                        resizeMode="stretch"
                        style={{
                            width: 80,
                            height: 96,
                            opacity: 1,
                            position: 'absolute',
                            right: 16,
                            top: 24,
                            ...Platform.select({
                                android: {
                                    top: 32,
                                }
                            })
                        }} />
                    
                    <View style={{
                        position: 'absolute',
                        right: 16,
                        top: 30 + 24,
                        backgroundColor: 'white',
                        borderRadius: 48,
                        ...Platform.select({
                            android: {
                                top: 30 + 32,
                            }
                        }),
                    }}>
                        <Image
                            source={constants.IMG_LOGO_UPC}
                            resizeMode="stretch"
                            style={{
                                width: 64,
                                height: 64,
                            }} />
                    </View>

                    <Text style={styles.featuredText}>Record{'\n'}Académico</Text>
                </LinearGradient>

                <FlatList
                    ListHeaderComponent={() => (
                        <Text h4 style={{
                            paddingHorizontal: 8,
                        }}>¡Hola! Aquí encontrarás todos los cursos que has aprobado.</Text>
                    )}
                    ListEmptyComponent={() => (
                        <Text h5 style={{
                            paddingHorizontal: 8,
                            textAlign: "center",
                            marginTop: 8,
                        }}>¡No hay información sobre tu record académico aún!</Text>
                    )}
                    contentContainerStyle={{ flexGrow: 1, paddingHorizontal: 8, }}
                    data={this.props.cursos_aprobados}
                    keyExtractor={(item, key) => key.toString()}
                    renderItem={({ item }) => (
                        <CursoAprobado
                            title={item.curso_nombre}
                            subtitle={'Curso Aprobado'}
                            imgSource={{
                                uri: item.curso_icono
                            }}
                            score={(item.curso_id*9)%21}
                            imgStyle={{
                                width: 48,
                                height: 48,
                            }}
                        />
                    )}
                />
               
            </View>
        );
    }

}


const styles = StyleSheet.create({
    mainView: {
        ...Style.StartMainView
    },
    gradientStyle: {
        ...Style.StartMainGradient
    },
    featuredText:{
        ...Style.StartFeaturedText,
    }
});


import { listCursosAprobados, setCursoActual } from '../../redux/actions';

const mapStateToProps = state => {
    return {
        estudiante_actual: state.estudiante_actual || {},
        cursos_aprobados: state.cursos_aprobados || [],
        loading: state.loading,
    };
};

const mapDispatchToProps = {
    listCursosAprobados
};


export default connect(mapStateToProps, mapDispatchToProps)( Record );
