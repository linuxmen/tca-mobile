import React from 'react';
import {
    StyleSheet,
    FlatList,
    View,
    Button,
    Alert,
    Image,
    TouchableHighlight,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView, 
    AsyncStorage,
    Platform,
    Dimensions,
} from 'react-native';

import { LinearGradient } from 'expo'

import CursoItem from '../../components/CursoItem'
import Text from '../../components/BaseText'
import Input from '../../components/BaseInput'
import GradientButton from '../../components/GradientButton'
import Loading from 'aula-upc/components/Loading'

import * as constants from '../../constants'
import Style from '../../stylesheet'
import {Button as RNEButton, Icon} from 'react-native-elements'

import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import _ from 'lodash';

import BaseViewWithBackButton from '../../components/BaseViewWithBackButton'
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'
import SelectInput from '@tele2/react-native-select-input';

const DeviceDimensions = Dimensions.get('window')

const PerfilRow = ({ text, icon, ...props }) => (
    <View style={{
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 8,
    }}>
        <View style={{
            width: 24,
            height: 24,
            backgroundColor: constants.COLOR_PRIMARY,
            borderRadius: 18,
            overflow: 'hidden',
            marginRight: 16,
            justifyContent: 'center',
            alignItems: 'center',
        }}>
            <Icon {...icon} />
        </View>
        <Text style={{
            paddingTop: 2,
        }}>{text}</Text>
    </View>
);

const PerfilEditRow = ({ text, icon, select = false, ...inputProps }) => {
    const EditComponent = select ? SelectInput : Input;

    return (<View style={{
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 8,
    }}>
        <View style={{
            width: 24,
            height: 24,
            backgroundColor: constants.COLOR_PRIMARY,
            borderRadius: 18,
            overflow: 'hidden',
            marginRight: 16,
            justifyContent: 'center',
            alignItems: 'center',
        }}>
            <Icon {...icon} />
        </View>
        
        <EditComponent
            value={text}
            {...inputProps}
            />
    </View>
    );
}

const PerfilEditSelectRow = ({ text, icon, ...inputProps }) => (
    <View style={{
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 8,
    }}>
        <View style={{
            width: 24,
            height: 24,
            backgroundColor: constants.COLOR_PRIMARY,
            borderRadius: 18,
            overflow: 'hidden',
            marginRight: 16,
            justifyContent: 'center',
            alignItems: 'center',
        }}>
            <Icon {...icon} />
        </View>
        <SelectInput 
            value={text}
            {...inputProps}
            />
    </View>
);

class Perfil extends React.Component {
    componentDidMount(){
        this.props.getPerfil(this.props.estudiante_actual.estudiante_correo);
        this.props.loadDistritos(this.props.estudiante_actual.estudiante_correo);
    }
    componentDidUpdate(){
        const est = this.props.estudiante_actual;
        var newState = {};

        if (!this.state.editando && this.state.estudiante_telefono != est.estudiante_telefono){
            newState.estudiante_telefono = est.estudiante_telefono;
        }
        if (!this.state.editando && this.state.estudiante_facebook != est.estudiante_facebook){
            newState.estudiante_facebook = est.estudiante_facebook;
        }
        if (!this.state.editando && this.state.estudiante_distrito != est.estudiante_distrito){
            newState.estudiante_distrito = est.estudiante_distrito;
        }
        if (!_.isEmpty(newState)) {
            this.setState(newState)
        }

    }
    state = {
        estudiante_telefono: '',
        estudiante_distrito: '',
        estudiante_facebook: '',
        editando: false,
    }

    editarPerfil(){
        this.props.editarPerfil(
            this.props.estudiante_actual.estudiante_correo,
            this.state
        );
    }

    btnEditar(){
        const perfil = this.props.estudiante_actual;

        var newstate = {
            editando: !this.state.editando
        }
        if (this.state.editando) {
            this.setState(newstate);
            this.editarPerfil();
            return;
        }
        this.setState(newstate);

    }

    render() {
        if (this.props.loading && !this.props.distritos) {
            return (<Loading message="Cargando perfil..."/>)
        }

        var perfil = this.props.estudiante_actual;

        var Component = this.state.editando ? PerfilEditRow : PerfilRow;
        var handler = (key) => {
                return (text) => {
                    var new_state = {}
                    new_state[key] = text
                    this.setState(new_state)
                }
            }
        var textfrom = (key) => {
            if(this.state.editando){
                return this.state[key]
            }
            return this.props.estudiante_actual[key]
        }

        var distritos = this.props.distritos.map((dist) => ({
            label: dist.distrito,
            value: dist.distrito_id,
        }))

        return (<KeyboardAwareScrollView
            enableOnAndroid={Platform.OS == 'android'}
            style={{
                flex: 1,
                backgroundColor: 'white',
            }}
            behavior="padding"
            >
            
            <View>
                <LinearGradient style={[styles.gradientStyle, {
                        alignItems: 'center',
                        justifyContent: 'center',
                        paddingBottom: 8,
                        paddingTop: 40,
                        width: '100%'
                    }]}
                    colors={[constants.COLOR_SECONDARY, constants.COLOR_PRIMARY]}
                    >
                    <Image
                        source={constants.ICONO_MEDALLA_OFF_GRANDE}
                        resizeMode="stretch"
                        style={{
                            width: 80,
                            height: 96,
                            position: 'absolute',
                            right: 16,
                            top: 24,
                        }} />
            
                    <View style={{
                        borderRadius: 48,
                        overflow: 'hidden',
                        marginBottom: 8,
                    }}>
                        <Image
                            source={perfil.estudiante_sexo == 'm' ? constants.DEFAULT_ICON_ALUMNO : constants.DEFAULT_ICON_ALUMNA}
                            style={{
                                width: 96,
                                height: 96,
                            }}
                            resizeMode="stretch"
                            />
                    </View>
                    <Text style={{
                        color: 'white',
                        textAlign: 'center',
                    }}>
                
                    {perfil.nombre_completo}
                    </Text>

                    <TouchableOpacity style={{
                        position: 'absolute',
                        top: 68,
                        zIndex: 4,
                        right: ((DeviceDimensions.width) / 2) - 72,
                        borderRadius: 48,
                        backgroundColor: 'white',
                        overflow: 'hidden',
                        elevation: 3,
                        width: 48,
                        height: 48,
                        padding: 8,
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}
                        onPress={() => {
                            Actions.push('recordEstudiante')
                        }}
                    >
                        <Image
                            source={constants.DEFAULT_ICON_RESENIA_EST}
                            resizeMode="stretch"
                            style={{
                                width: 24,
                                height: 32,
                            }}
                        />
                    </TouchableOpacity>
                </LinearGradient>
                
                <View style={{
                    paddingHorizontal: 8,
                    }}>

                    <PerfilRow
                        icon={{
                            name: 'email-outline',
                            type: 'material-community',
                            color: 'white',
                            size: 16,
                        }}
                        text={perfil.estudiante_correo}
                    />

                    <Component
                        icon={{
                            name: 'cellphone-iphone',
                            type: 'material-community',
                            color: 'white',
                            size: 16,
                        }}
                        onChangeText={handler('estudiante_telefono')} 
                        text={this.state.estudiante_telefono} 
                        />
                    <Component 
                        icon={{
                            name: 'location',
                            type: 'entypo',
                            color: 'white',
                            size: 14,
                        }}
                        select={true} 
                        options={distritos}
                        placeholder="Seleccione distrito..."
                        onChange={handler('estudiante_distrito')} 
                        value={this.state.estudiante_distrito} 
                        text={perfil.distrito} 
                        />
                    <Component 
                        icon={{
                            name: 'sc-facebook',
                            type: 'evilicon',
                            color: 'white',
                            size: 18,
                        }}
                        onChangeText={handler('estudiante_facebook')} 
                        text={this.state.estudiante_facebook} 
                        />
                </View>

                <View style={{
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                    }}>
                    <View style={{
                        paddingHorizontal: 8,
                        width: 150,
                        paddingBottom: 8,
                    }}>
                        <GradientButton
                            text="Editar Perfil"
                            textProps={{
                                h5: true,
                            }}
                            style={{
                                paddingHorizontal: 4,
                                paddingVertical: 6,
                                borderRadius: 8,
                                overflow: 'hidden'
                            }}
                            textStyle={{
                                color: 'white',
                                textAlign: 'center',
                            }}
                            onPress={() => {
                                this.btnEditar()   
                            }}
                            colors={[constants.COLOR_TERTIARY, constants.COLOR_SECONDARY]}
                            start={{ x: 0, y: 0 }}
                            end={{ x: 1, y: 0 }}
                        />
                    </View>
                </View>
                <View style={{
                    marginVertical: 16
                    }}>
                    <Text style={{
                        marginBottom: 8,
                        textAlign: 'center',
                    }}>
                        ¿Alguna Sugerencia?
                    </Text>
                    
                    <TouchableOpacity style={{
                        justifyContent: 'flex-start',
                        alignItems: 'center',
                        zIndex: 3,
                    }}
                    onPress={() => {
                        Actions.push('feedback')
                    }}
                    >
                        <View style={{
                            borderRadius: 48,
                            backgroundColor: constants.COLOR_SECONDARY,
                            overflow: 'hidden',
                            width: 64,
                            height: 64,
                            justifyContent: 'flex-start',
                            alignItems: 'center',
                            elevation: 5,
                            padding: 16,
                        }}>
                            <Image
                                source={constants.ICONO_FLECHA_FEEDBACK}
                                resizeMode="stretch"
                                style={{
                                    width: 32,
                                    height: 32,
                                }}
                            />

                        </View>
                    </TouchableOpacity>
                </View>

                <View style={{
                    width: '100%',
                    paddingBottom: 16,
                    }}>
                    <GradientButton
                        text="Cerrar Sesión"
                        textProps={{
                            h5: true,
                        }}
                        style={{
                            marginTop: 32,
                            marginHorizontal: 32,
                            paddingHorizontal: 4,
                            paddingVertical: 6,
                            borderRadius: 8,
                            overflow: 'hidden'
                        }}
                        textStyle={{
                            color: 'white',
                            textAlign: 'center',
                        }}
                        onPress={async () => {
                            await AsyncStorage.setItem('log', '0');
                            Actions.reset('login')
                        }}
                        colors={[constants.COLOR_TERTIARY, constants.COLOR_SECONDARY]}
                        start={{ x: 0, y: 0 }}
                        end={{ x: 1, y: 0 }}
                    />
                </View>
            </View>
        </KeyboardAwareScrollView>
        );
    }

}


const styles = StyleSheet.create({
    mainView: {
        ...Style.LoginView,
        backgroundColor: 'white',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        // height: '100%',
    },
    gradientStyle: {
        ...Style.StartMainGradient
    },
    featuredText:{
        ...Style.StartFeaturedText
    }
});


import { getPerfil, editarPerfil, loadDistritos } from '../../redux/actions';

const mapStateToProps = state => {
    return {
        estudiante_actual: state.estudiante_actual || {},
        loading: state.loading,
        distritos: state.distritos,
    };
};

const mapDispatchToProps = {
    getPerfil, editarPerfil, loadDistritos
};


export default connect(mapStateToProps, mapDispatchToProps)(Perfil);
