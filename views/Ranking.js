import React from 'react';
import {
    StyleSheet,
    FlatList,
    View,
    Button,
    Alert,
    Image,
    TouchableHighlight,
} from 'react-native';

import { fromJS, Map } from 'immutable';
import { createStackNavigator } from 'react-navigation';
import { LinearGradient } from 'expo'

import ProfesorItem from '../components/ProfesorItem'
import Text from '../components/BaseText'
import Loading from 'aula-upc/components/Loading'

import BaseViewWithBackButton from '../components/BaseViewWithBackButton';
import Style from '../stylesheet'
import * as constants from '../constants'
import { Button as RNEButton, ButtonGroup } from 'react-native-elements'

import { get } from 'lodash'

class Ranking extends React.Component {
    componentDidMount() {
        this.sendRequest();
    }
    
    sendRequest(){
        var dispatchers = [
            this.props.rankingCurso,
            this.props.rankingRecomendado,
        ]
    
        dispatchers[this.state.selected](this.props.estudiante_actual.estudiante_correo);
    }

    state = {
        selected: 0,
    }

    render() {
        if (this.props.loading) {
            return (<Loading message="Cargando profesores..." />)
        }

        return (
            <View style={styles.mainView}>
                <LinearGradient style={styles.gradientStyle}
                    colors={[constants.COLOR_TERTIARY, constants.COLOR_SECONDARY]}
                >
                    <Image
                        source={constants.ATOMO_LISTA_PROFESOR}
                        resizeMode="stretch"
                        style={{
                            width: 80,
                            height: 86,
                            opacity: 0.5,
                            position: 'absolute',
                            right: 16,
                            top: 24,
                        }} />

                    <Text style={styles.featuredText}>Ranking</Text>
                    <ButtonGroup
                        buttons={['Por Curso', 'Recomendado']}
                        onPress={(selected) => {
                            this.setState({ selected });
                            this.sendRequest();
                        }}
                        containerStyle={{
                            backgroundColor: 'transparent',
                            height: 32
                        }}
                        selectedIndex={this.state.selected}
                        selectedTextStyle={{
                            color: constants.COLOR_PRIMARY,
                        }}
                        selectedButtonStyle={{
                            backgroundColor: 'white',
                        }}
                        textStyle={{
                            color: 'white',
                            fontWeight: '400'
                        }}
                        buttonStyle={{
                            backgroundColor: 'transparent',
                            paddingVertical: 0
                        }}
                        />
                </LinearGradient>

                <FlatList
                    contentContainerStyle={{ flexGrow: 1, paddingHorizontal: 8, }}
                    data={this.props.ranking}
                    keyExtractor={(item, key) => key.toString()}
                    renderItem={({ item, index}) => {
                        var imgPerfil = get(item, 'profesor_pic');

                        if (imgPerfil) {
                            imgPerfil = {
                                uri: imgPerfil
                            }
                        } else {
                            var masculino = get(item, 'profesor_sexo') == 'm';

                            imgPerfil = masculino
                                ? constants.DEFAULT_ICON_PROFESOR
                                : constants.DEFAULT_ICON_PROFESORA
                        }

                        console.log(imgPerfil);

                        return (
                        <ProfesorItem
                            title={item.nombre_completo}
                            rightBgColor={constants.COLOR_SECONDARY}
                            rightText={(index + 1).toString()}
                            rightTextStyle={{
                                color: 'white',
                            }}
                            subtitle={item.curso_nombre ? item.curso_nombre : null}
                            onPress={() => {
                                this.props.setProfesorActual(item);
                                this.props.setCursoActual(item);
                                this.props.setLoading(true);
                                Actions.perfilProfesorCurso();
                            }}
                            ranking={parseFloat(item.prom_calificacion)}
                            imgSource={imgPerfil}
                        />
                    )}}
                />

            </View>
        );
    }

}


const styles = StyleSheet.create({
    mainView: {
        ...Style.StartMainView
    },
    gradientStyle: {
        ...Style.StartMainGradient
    },
    featuredText: {
        ...Style.StartFeaturedText
    }
});

import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

import { rankingCurso, rankingRecomendado, rankingVotado, setProfesorActual, setCursoActual, setLoading} from '../redux/actions';

const mapStateToProps = state => {
    return {
        estudiante_actual: state.estudiante_actual || {},
        ranking: state.ranking || [],
        loading: state.loading,
    };
};

const mapDispatchToProps = {
    rankingCurso, rankingRecomendado, rankingVotado,
    setProfesorActual, setCursoActual, setLoading
};


export default connect(mapStateToProps, mapDispatchToProps)(Ranking);
