import React from 'react';
import {
    StyleSheet,
    View,
    Image,
    Alert,
    KeyboardAvoidingView,
    ActivityIndicator,
    StatusBar,
    Dimensions,
    Platform,
    AsyncStorage,
    TouchableOpacity,
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import GradientButton from '../../components/GradientButton'
import Loading from '../../components/Loading'

import { LinearGradient } from 'expo';
import Text from '../../components/BaseText'
import Input from '../../components/BaseInput'
import Button from '../../components/BaseButton'

const DEVICE_WIDTH = Dimensions.get("window").width

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: 'usuario@upc.edu.pe',
            password:  '123456',
        }
    }
    passwordInput = null;

    onChangeEmail(){
        return;
        if (this.passwordInput)
            this.passwordInput.focus()
    }

    showError(msg){
        Alert.alert(
            'Error',
            msg
        )
    }

    async componentDidUpdate(){
        if(!this.props.side_effect){
            return;
        }
        
        if (this.props.side_effect == 'STORE_LOGIN') {
            await AsyncStorage.setItem('log', '1');
            await AsyncStorage.setItem('last_log', (new Date()).getTime().toString());
            await AsyncStorage.setItem('estudiante_actual', JSON.stringify(this.props.estudiante_actual));
            
            this.setState({
                email: '',
                password: '',
            });
            
            this.props.clearSideEffects();
            this.props.setLoading(true);
            Actions.reset('authFlow')
            return;
        }
        
        if (this.props.side_effect == 'LOGIN_FAILED') {
            // this.props.clearSideEffects();
            this.showError('Correo/clave inválido. Por favor, verifica los datos ingresados.');
            this.props.clearSideEffects();
            return;
        }
        
        // if (this.props.side_effect == 'LOAD_LOGIN') {
        //     var lastLogged = await AsyncStorage.getItem('last_log') || '0';
        //     var estudiante_actual = await AsyncStorage.getItem('estudiante_actual') || 'null';
        //     estudiante_actual = JSON.parse(estudiante_actual)
        //     lastLogged = parseFloat(lastLogged);
        //     this.props.storeLogin(estudiante_actual);
            
        //     var now = (new Date()).getTime();
        //     var diff = ((now - lastLogged) /1000) / 3600;
            
        //     if (diff < 1 && estudiante_actual.estudiante_correo) {
        //         this.props.setLoading(true);
        //         Actions.reset('authFlow')
        //     }
            
        //     this.props.clearSideEffects();
        //     return;
        // }
    }

    async componentDidMount(){
        // var isLogged = await AsyncStorage.getItem('log') == '1';
        // if (isLogged){
        //     this.props.hackSetLoadLogin();
        // }
    }

    async doIngresar(){
        if (!(this.state.email && this.state.password)) {
            this.showError('Por favor, ingrese su correo institucional y su contraseña');
            return;
        }
        
        var emailRegex = /[a-z0-9_][a-z0-9_\.]+@[a-z0-9_][a-z0-9_\.]+[a-z0-9_]\.[a-z]{2,}/;

        if (!emailRegex.test(this.state.email)) {
            this.showError('Por favor, ingrese un correo institucional válido.');
            return;
        }
        
        if (this.state.password.length < 5) {
            this.showError('Por favor, su contraseña debe contener al menos 6 caracteres.');
            return;
        }
        
        this.props.login(this.state.email, this.state.password)

    }

    render() {
        if (this.props.loading) {
            return (<Loading message="Enviando datos..."/>)
        }
        return (
            <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={5} enabled style={styles.mainView}>
                <View style={styles.logoWrapper}>
                    <Image 
                        style={styles.logoImg}
                        source={constants.ATOMO_LOGIN} />
                </View>

                <View style={{
                    paddingHorizontal: 40,
                    marginBottom: 43
                }}>
                    <Image source={constants.ATOMO_LOGIN_PRIMARY} 
                        resizeMethod="scale"
                        resizeMode="stretch"
                        style={{
                        width: DEVICE_WIDTH*0.75,
                        height: DEVICE_WIDTH * 0.3
                        }} />
                </View>

                <View style={{ paddingHorizontal: 20, marginBottom: 12 }}>
                    <Text style={{ color: 'white', textAlign:'center', fontSize: 16 }}>
                        Ingresa con tu correo y contraseña institucional.
                    </Text>
                </View>

                <View style={styles.formWrapper}>
                    <Input underlineColorAndroid="transparent" 
                        placeholder="Ingresa tu correo institucional"
                        inputStyle={styles.inputStyle}
                        containerStyle={{alignSelf: 'center'}}
                        inputContainerStyle={styles.inputContainerStyle} 
                        autoCapitalize="none"
                        keyboardAppearance="light"
                        keyboardType="email-address"
                        autoCorrect={false}
                        returnKeyType="next"
                        blurOnSubmit={true}
                        onChangeText={(text) => { this.setState({'email': text})} }
                        value={this.state.email}
                        onSubmitEditing={() => { this.onChangeEmail() }}
                        />
                    
                    <Input underlineColorAndroid="transparent" 
                        ref={(input) => (this.passwordInput = input)}
                        autoCorrect={false}
                        keyboardAppearance="light"
                        placeholder="Ingresa tu contraseña  "
                        autoCapitalize="none"
                        secureTextEntry={true}
                        inputStyle={styles.inputStyle}
                        containerStyle={{alignSelf: 'center'}}
                        inputContainerStyle={styles.inputContainerStyle} 
                        onChangeText={(text) => { this.setState({'password': text})} }
                        value={this.state.password}
                    />
                    <Button 
                        buttonStyle={styles.loginBtn} 
                        onPress={() => this.doIngresar()}
                        title="Ingresar"
                        />
                </View>
            </KeyboardAvoidingView>
        );
    }
}

import Style from '../../stylesheet';
import * as constants from '../../constants';



const styles = StyleSheet.create({
    mainView: {
        ...Style.LoginView,
    },
    gradientStyles:{
        ...Style.LoginGradient,
    },
    inputStyle: {
        paddingRight: 20,
        // fontSize: 18,
    },
    loginText: {
        ...Style.LoginText,
    },
    inputContainerStyle: { 
        marginVertical: 3,
        borderRadius: 10,
        backgroundColor: "white",
        paddingVertical: 10,
    },
    loginBtn: {
        ...Style.LoginButton,
    },
    formWrapper: {
        width: '100%',
    },
    logoWrapper: Style.LogoWrapper,
    logoImg: {
        opacity: 0.5,
        alignSelf: 'flex-end',
    },
});

import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

import { login, hackSetLoadLogin, clearSideEffects, storeLogin, setLoading} from '../../redux/actions';

const mapStateToProps = state => {
    return {
        estudiante_actual: state.estudiante_actual,
        loading: state.loading,
        side_effect: state.side_effect,
        error: state.error,
    };
};

const mapDispatchToProps = {
    login,
    hackSetLoadLogin,
    clearSideEffects,
    storeLogin,
    setLoading,
};


export default connect(mapStateToProps, mapDispatchToProps)(Login);
