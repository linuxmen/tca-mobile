import React from 'react';
import {
    StyleSheet,
    FlatList,
    View,
    Button,
    Alert,
    Image,
    TouchableHighlight,
} from 'react-native';

import { fromJS, Map } from 'immutable';
import { createStackNavigator } from 'react-navigation';
import { LinearGradient } from 'expo'

import MensajeItem from '../../components/MensajeItem'
import Text from '../../components/BaseText'
import Loading from 'aula-upc/components/Loading'

import BaseViewWithBackButton from '../../components/BaseViewWithBackButton';
import Style from '../../stylesheet'
import * as constants from '../../constants'
import { Button as RNEButton } from 'react-native-elements'

class MainMensajes extends React.Component {
    componentDidMount() {
        this.props.getMensajes(
            this.props.estudiante_actual.estudiante_correo,
        );
    }

    renderEmpty() {
        return (<View style={{
            flex: 1,
            paddingHorizontal: 16,
        }}>
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignContent: 'space-between',
            }}>
                <Text style={{ textAlign: 'center', color: '#AAAAAA' }} h1>:(</Text>
                <Text style={{ textAlign: 'center', color: '#AAAAAA' }}>¡Todavia no tienes mensajes!</Text>
                <Text style={{ textAlign: 'center', color: '#AAAAAA' }}>Da el primer paso y enviale un mensaje a un compañero del campus.</Text>
            </View>
            <View>
                <Image
                    source={constants.IMG_GATOS_REPETIDOS}
                    resizeMode="contain"
                    style={{
                        width: '100%',
                    }}
                />
            </View>
        </View>)
    }

    render() {
        if (this.props.loading) {
            return (<Loading message="Cargando Mensajes..." />)
        }
        const mensajes = this.props.mensajes;
        return (
            <View style={styles.mainView}>
                <LinearGradient style={styles.gradientStyle}
                    colors={[constants.COLOR_SECONDARY, constants.COLOR_TERTIARY]}
                >
                    <Image
                        source={constants.ICONO_MEDALLA_OFF_GRANDE}
                        resizeMode="stretch"
                        style={{
                            width: 80,
                            height: 96,
                            position: 'absolute',
                            right: 16,
                            top: 24,
                        }} />
                    <Text style={styles.featuredText}>Inbox</Text>
                </LinearGradient>
                {mensajes.length == 0 && this.renderEmpty()}

                {mensajes.length > 0 && (
                    <FlatList
                        contentContainerStyle={{ flexGrow: 1, paddingHorizontal: 8, }}
                        data={mensajes}
                        keyExtractor={(item, key) => key.toString()}
                        renderItem={({ item }) => (
                            <MensajeItem
                                title={(item.otro && item.otro.nombre_completo) || null}
                                subtitle={item.conversacion_ultimo_mensaje}
                                onPress={() => {
                                    this.props.setChatActual(item);
                                    Actions.push('viewChat')
                                }}
                                ranking={item.calificacion}
                                imgSource={(item.otro && item.otro.estudiante_sexo == 'm' ? (constants.DEFAULT_ICON_ALUMNO) : (constants.DEFAULT_ICON_ALUMNA)) || undefined}
                            />
                        )}
                    />
                )}

            </View>
        );
    }

}


const styles = StyleSheet.create({
    mainView: {
        ...Style.StartMainView
    },
    gradientStyle: {
        ...Style.StartMainGradient
    },
    featuredText: {
        ...Style.StartFeaturedText
    }
});

import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

import { getMensajes, setChatActual, getConversacion } from '../../redux/actions';

const mapStateToProps = state => {
    return {
        estudiante_actual: state.estudiante_actual || {},
        mensajes: state.mensajes || [],
        loading: state.loading,
    };
};

const mapDispatchToProps = {
    getMensajes,
    setChatActual,
    getConversacion
};


export default connect(mapStateToProps, mapDispatchToProps)(MainMensajes);
