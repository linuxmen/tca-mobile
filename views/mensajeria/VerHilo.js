import React from 'react';
import {
    StyleSheet,
    FlatList,
    View,
    Button,
    Alert,
    Image,
    KeyboardAvoidingView,
    TouchableOpacity,
} from 'react-native';

import { fromJS, Map } from 'immutable';
import { createStackNavigator } from 'react-navigation';
import { LinearGradient } from 'expo'

import ChatItem from '../../components/ChatItem'
import Text from '../../components/BaseText'
import Input from '../../components/BaseInput'
import Loading from 'aula-upc/components/Loading'

import BaseViewWithBackButton from '../../components/BaseViewWithBackButton';
import Style from '../../stylesheet'
import * as constants from '../../constants'
import { Button as RNEButton } from 'react-native-elements'

class VerHilo extends React.Component {
    componentDidMount() {
        this.props.clearSideEffects();

        this.props.getConversacion(
            this.props.estudiante_actual.estudiante_correo,
            this.props.chat_actual.conversacion_id,
        );
    }
    state = {
        texto: '',
    }
    scrollView = null
    
    componentDidUpdate(){
        if (this.scrollView)
            setTimeout(() => {
                this.scrollView.scrollToEnd({
                    animated: false
                });
            }, 200)
    }

    sendMensaje(){
        this.props.sendMensaje(
            this.props.estudiante_actual.estudiante_correo,
            this.props.chat_actual.conversacion_id, 
            this.state.texto
        )
        this.setState({texto: ''});
    }
    
    render() {
        if (this.props.loading) {
            return (<Loading message="Enviando mensaje..." />)
        }

        const mensajes = this.props.conversacion

        return (
            <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={5} enabled style={styles.mainView}>
                <LinearGradient style={styles.gradientStyle}
                    colors={[constants.COLOR_SECONDARY, constants.COLOR_TERTIARY]}
                >
                    <Image
                        source={constants.ICONO_MEDALLA_OFF}
                        resizeMode="stretch"
                        style={{
                            width: 80,
                            height: 96,
                            opacity: 0.15,
                            position: 'absolute',
                            right: 16,
                            top: 24,
                        }} />
                    <Text style={styles.featuredText}>Inbox</Text>
                </LinearGradient>

                <FlatList
                    ref={(ref) => { this.scrollView = ref; }} 
                    contentContainerStyle={{ flexGrow: 1, paddingHorizontal: 8, }}
                    data={mensajes}
                    keyExtractor={(item, key) => key.toString()}
                    renderItem={({ item }) => (
                        <ChatItem
                            right={item.estudiante_id == this.props.estudiante_actual.estudiante_id}
                            message={item.mensaje_texto}
                        />
                    )}
                    />
                <View style={{
                    backgroundColor: '#e3f0fc'
                }}>
                    <Input placeholder="Escribe aquí..." 
                        inputStyle={{
                            padding: 0,
                            margin: 0,
                        }} 
                        value={this.state.texto}
                        onChangeText={(texto) => {
                            this.setState({texto})
                        }}
                        inputContainerStyle={{
                            borderBottomWidth: 0,
                        }}
                        containerStyle={{
                            paddingRight: 8,
                            width: '100%',
                        }} 
                        rightIcon={(
                            <TouchableOpacity activeOpacity={0.2} onPress={() => {
                                this.sendMensaje()
                            }}>
                                <Image style={{
                                        width: 24,
                                        height: 20,
                                    }}
                                    resizeMode="stretch"
                                    source={constants.ICONO_FLECHA_CHAT}
                                    />
                            </TouchableOpacity>
                        )}
                    />
                </View>

            </KeyboardAvoidingView>
        );
    }

}


const styles = StyleSheet.create({
    mainView: {
        ...Style.StartMainView
    },
    gradientStyle: {
        ...Style.StartMainGradient
    },
    featuredText: {
        ...Style.StartFeaturedText
    }
});

import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

import { getConversacion, sendMensaje, clearSideEffects } from '../../redux/actions';

const mapStateToProps = state => {
    return {
        estudiante_actual: state.estudiante_actual || {},
        chat_actual: state.chat_actual || {},
        conversacion: state.conversacion || [],
        loading: state.loading,
    };
};

const mapDispatchToProps = {
    getConversacion,
    sendMensaje,
    clearSideEffects
};


export default connect(mapStateToProps, mapDispatchToProps)(VerHilo);
