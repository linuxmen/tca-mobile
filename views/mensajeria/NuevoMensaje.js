import React from 'react';
import {
    StyleSheet,
    FlatList,
    View,
    Alert,
    Image,
    TouchableHighlight,
} from 'react-native';

import { fromJS, Map } from 'immutable';
import { createStackNavigator } from 'react-navigation';
import { LinearGradient } from 'expo'

import Text from '../../components/BaseText'
import Loading from 'aula-upc/components/Loading'

import Style from '../../stylesheet'
import * as constants from '../../constants'
import { Button as RNEButton } from 'react-native-elements'

import Input from '../../components/BaseInput'
import Button from '../../components/BaseButton'

class NuevoMensaje extends React.Component {
    componentDidMount() {

    }

    state = {
        texto: '',
    }

    doIngresar(){
        this.props.iniciarConversacion(
            this.props.estudiante_actual.estudiante_correo,
            this.props.comentario_actual.inscripcion_id, 
            this.state
        );
        this.props.getMensajes(
            this.props.estudiante_actual.estudiante_correo
        );
    }

    componentDidUpdate(){
        if (this.props.side_effect == 'REDIRECT_CHAT') {
            this.props.clearSideEffects();
            Actions.viewChat();
        }
    }

    render() {
        if (this.props.loading) {
            return (<Loading message="Enviando mensaje..." />)
        }
        return (
            <View style={styles.mainView}>
                <LinearGradient style={styles.gradientStyle}
                    colors={[constants.COLOR_PRIMARY, constants.COLOR_SECONDARY]}
                >
                    <Image
                        source={constants.ICONO_MEDALLA_COLOR_GRANDE}
                        resizeMode="stretch"
                        style={{
                            width: 80,
                            height: 86,
                            opacity: 0.5,
                            position: 'absolute',
                            right: 16,
                            top: 24,
                        }} />

                    <Text style={styles.featuredText}>Nuevo Mensaje</Text>
                </LinearGradient>
                <View style={styles.formWrapper}>
                    <Input underlineColorAndroid="transparent"
                        placeholder="Escribe aquí tu mensaje..."
                        inputStyle={styles.inputStyle}
                        containerStyle={{ alignSelf: 'center' }}
                        inputContainerStyle={styles.inputContainerStyle}
                        autoCapitalize="none"
                        keyboardAppearance="light"
                        keyboardType="default"
                        autoCorrect={false}
                        multiline={true}
                        returnKeyType="next"
                        blurOnSubmit={true}
                        onChangeText={(text) => { this.setState({ 'texto': text }) }}
                        value={this.state.texto}
                    />

                    <Button
                        buttonStyle={styles.loginBtn}
                        onPress={() => this.doIngresar()}
                        title="Enviar"
                    >
                    </Button>
                </View>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    mainView: {
        ...Style.StartMainView
    },
    gradientStyle: {
        ...Style.StartMainGradient
    },
    featuredText: {
        ...Style.StartFeaturedText
    },
    inputStyle: {
        paddingRight: 20,
        height: 120,
        // fontSize: 18,
    },
    inputContainerStyle: {
        marginVertical: 3,
        borderRadius: 7,
        backgroundColor: "white",
        paddingVertical: 10,
        // borderBottomWidth: 0,
    },
    formWrapper: {
        width: '100%',
    },
});

import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

import { iniciarConversacion, clearSideEffects, getMensajes } from '../../redux/actions';

const mapStateToProps = state => {
    return {
        estudiante_actual: state.estudiante_actual || {},
        comentario_actual: state.comentario_actual || {},
        side_effect: state.side_effect,

        comentarios: state.comentarios || [],
        loading: state.loading,
    };
};

const mapDispatchToProps = {
    iniciarConversacion,
    clearSideEffects,
    getMensajes,
};


export default connect(mapStateToProps, mapDispatchToProps)(NuevoMensaje);
