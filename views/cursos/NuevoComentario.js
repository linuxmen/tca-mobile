import React from 'react';
import {
    StyleSheet,
    View,
    Alert,
} from 'react-native';

import * as constants from 'aula-upc/constants'
import Store from 'aula-upc/redux/store'
import WebAPI from 'aula-upc/webapi'
import {Input, Button} from 'react-native-elements';
import Loading from 'aula-upc/components/Loading';

import BaseView from 'aula-upc/components/BaseView';

export default class NuevoComentario extends BaseView {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            mensaje: '',
        }
    }

    static navigationOptions = {
        title: 'Nuevo Comentario',
        headerStyle: {
            backgroundColor: constants.primaryBgColor
        },
        headerTitleStyle: {
            color: constants.bgColor,
        },
        headerTintColor: constants.bgColor,
    }
    
    async _onPress(){
        this.setState({
            loading: true,
        });

        let inscripcionActual = Store.get('current.inscripcion_id', '');

        try{
            var comentarios = await WebAPI.sendComentario(
                inscripcionActual, this.state.mensaje
            )

            Store.mutate((store) => {
                var current = store.get('current').set('puedeComentar', false);
                return store.set('current', current);
            })
            this.props.navigation.setParams({'now': new Date()});

            Alert.alert(
                'Información',
                'Tu comentario se ha enviado exitosamente. En breve será publicado al aprobar la moderación.',
                [
                  {text: 'OK', onPress: () => {
                        this.setState({
                            mensaje: '',
                            loading: false,
                        });
                        this.props.navigation.goBack()
                    }},
                ],
                { cancelable: false }
              )
            
        }catch(e){
            // console.log(e);
            // console.error('ERROR:', e);
        }
        
    }

    render() {
        if (this.state.loading) {
            return (<Loading message="Enviando datos..."/>)
        }
        return (
            <View style={styles.mainView}>
                <Input underlineColorAndroid="transparent" 
                    ref={(input) => (this.mensaje = input)}
                    onChangeText={(text) => { this.setState({'mensaje': text})} }
                    value={this.state.mensaje}
                    multiline={true}
                    numberOfLines={6}
                    // keyboardAppearance="light"
                    placeholder="Escribe aquí tu comentario..."
                    inputStyle={{
                        textAlignVertical: 'top',
                        height: 160,
                    }}
                    label="Mensaje"
                    containerStyle={{
                        alignSelf: 'center',
                    }}
                    labelStyle={{
                        color: constants.primaryBgColor,
                    }}
                    inputContainerStyle={{
                        borderBottomWidth: 0,
                        borderTopColor: constants.primaryBgColor,
                        borderTopWidth: 1
                    }} 
                    />
                
                <View style={{width: 100, alignSelf: 'center', }}>
                    <Button 
                        title="Enviar" 
                        onPress={() => this._onPress()} 
                        buttonStyle={{
                            backgroundColor: constants.primaryBgColor
                        }}
                        />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    mainView: {
        backgroundColor: constants.bgColor,
        paddingTop: 20,
        height: '100%',
        width: '100%',
    },
});