import React from 'react';
import {
    StyleSheet,
    View,
    StatusBar,
    Dimensions,
    FlatList
} from 'react-native';

import { ListItem } from 'react-native-elements'

import * as constants from 'aula-upc/constants'
import Icon from 'react-native-vector-icons/Foundation';
import CursoSeleccionado from './CursoSeleccionado';
import ProfesorCurso from './ProfesorCurso';
import ComentariosProfesorCurso from './ComentariosProfesorCurso';
import NuevoComentario from './NuevoComentario';
import NuevoHilo from './NuevoHilo';
import Calificar from './Calificar';
import EstadisticaProfesorCurso from './EstadisticaProfesorCurso';

import { createStackNavigator } from 'react-navigation';

import Store from 'aula-upc/redux/store';
import WebAPI from 'aula-upc/webapi';
import BaseView from 'aula-upc/components/BaseView';
import {fromJS, List} from 'immutable';
import Loading from 'aula-upc/components/Loading'

const SCREEN_WIDTH = Dimensions.get('window').width;

class Cursos extends BaseView {
    constructor(props) {
        super(props);
        this.state = {
            cursos: List(),
            loading: true,
        }

        this.renderItem = this.renderItem.bind(this);
    }
    
    async fetchFromWebService(){
        var estudianteActual = Store.get('perfil')

        var listaCursos = await WebAPI.getCursos(
            estudianteActual.get('estudiante_id')
        );
        
        Store.mutate((state) => {
            return state.set('cursos', fromJS(listaCursos))
        })

        this.setState({
            loading: false,
        })
    }

    storeChanged(){
        this.setState({
            cursos: Store.get('cursos', List()),
        })
    }

    static navigationOptions = {
        title: 'Mis Cursos',
        headerStyle: {
            backgroundColor: constants.primaryBgColor
        },
        headerTitleStyle: {
            color: constants.bgColor,
        },
        tabBarIcon: ({ focused }) => {
            var color = "#666";
            if(focused){
                color = "#ccc";
            }
            return (<Icon name="book" size={28} color={color} />)
        }
    }
    
    navigate( materia ){
        Store.mutate(() => ({
            current: {
                curso: materia
            }
        }))
        this.props.navigation.navigate('CursoSeleccionado')
    }

    keyExtractor(item, index){
        return index.toString();
    }

    renderItem({ item }){
        return (<ListItem 
            onPress={() => this.navigate(item) }
            underlayColor={constants.primaryDarkBgColor}
            bottomDivider={true}
            leftAvatar={{
                source:{uri: item.get('curso_icono')}
            }} 
            title={item.get('curso_nombre')}
            rightIcon={{ color: constants.primaryBgColor, size: 32, name: 'chevron-right' }}
            containerStyle={{ padding: 8}}
            />)
    }
    render() {
        if (this.state.loading) {
            return (<Loading message="Cargando cursos..."/>)
        }

        const cursos = this.state.cursos

        return (
            <View style={styles.mainView}>
                <StatusBar backgroundColor="red" />
                <FlatList 
                    data={cursos.toArray()} 
                    renderItem={this.renderItem} 
                    keyExtractor={this.keyExtractor}
                    />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    mainView: {
        backgroundColor: constants.bgColor,
        height: '100%',
    },
    ingresaText: {
        alignSelf: 'center',
        marginBottom: 10,
    },
    parrafoText: {
        paddingHorizontal: 15,
        marginBottom: 15
    },
    logoImg: {
        width: SCREEN_WIDTH - 40,
        alignSelf: 'center',
    },
    cargandoStyles: {
        backgroundColor: constants.bgColor, 
        height: '100%',
        alignContent: 'center',
        justifyContent: 'center',
        width: '100%'
    },
});

var cursoStack = createStackNavigator({
    Cursos,
    CursoSeleccionado,
    ProfesorCurso,
    ComentariosProfesorCurso,
    NuevoComentario,
    Calificar,
    NuevoHilo,
    EstadisticaProfesorCurso
},{
    initialRouteName: 'Cursos',
    // headerMode: 'none',
})

import {Easing, Animated} from 'react-native';

cursoStack.navigationOptions = ({ navigation }) => ({
    title: 'Cursos',
    tabBarVisible: navigation.state.routes[navigation.state.index].routeName == 'Cursos',
    tabBarIcon: constants.tabIcon({
        name: 'book',
        type: 'font-awesome',
    }),
    headerBackTitle: '',
    transitionConfig: () => ({
        transitionSpec: {
          duration: 150,
          easing: Easing.out(Easing.poly(4)),
          timing: Animated.timing,
        },
    })
})
  
export default cursoStack