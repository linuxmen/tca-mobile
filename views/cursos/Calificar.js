import React from 'react';

import {
    Alert,
    Image,
    StyleSheet,
    View,
    StatusBar,
    ScrollView,
    FlatList
} from 'react-native';

import * as constants from '../../constants'

import {Badge, Icon, Text as TextRNE, Button} from 'react-native-elements';

import Loading from 'aula-upc/components/Loading'
import CalificadorWidget from 'aula-upc/components/CalificadorWidget'
import Text from 'aula-upc/components/BaseText'
import GradientButton from 'aula-upc/components/GradientButton'
import Input from '../../components/BaseInput'

import SelectInput from '@tele2/react-native-select-input';

class Calificar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hashtags_seleccionados: {},
            respuestas: {},
            tomar_denuevo: false,
            dificultad: 1,
            calificacion: 1,
            hashtags_cuenta: 0,
            comentario: '',
            grado_obtenido: '',
            carrera_cursando: '',
        }
    }
    componentDidMount() {
        this.props.getPreguntas(this.props.estudiante_actual.estudiante_correo);
        this.props.getHashtags(this.props.estudiante_actual.estudiante_correo);
        this.props.getGrados(this.props.estudiante_actual.estudiante_correo);
        this.props.getCarreras(this.props.estudiante_actual.estudiante_correo);
    }
    async doCalificar(){
        console.log(this.state);
        
        if(!this.state.comentario){
            Alert.alert(
                'Error',
                'Por favor indica tu comentario'
            );
            return;
        }
        if(!this.state.grado_obtenido){
            Alert.alert(
                'Error',
                'Por favor indica tu grado obtenido'
            );
            return;
        }
        if(!this.state.carrera_cursando){
            Alert.alert(
                'Error',
                'Por favor indica la carrera que cursas'
            );
            return;
        }
        this.props.calificar(this.props.estudiante_actual.estudiante_correo, this.props.perfil_profesor_actual.inscripcion_id, this.state);
    }

    setHashtag(item) {

        var hashtagsSeleccionados = this.state.hashtags_seleccionados;
        
        var hashtagId = item.hashtag_id;
        var value = hashtagsSeleccionados[hashtagId] || false;
        var newValue = !value;

        var hastagCount = this.state.hashtags_cuenta;
        
        if(newValue){
            hastagCount++;
        }else{
            hastagCount--;
        }

        if(hastagCount > 3){
            return;
        }

        var newHastags = Object.assign({}, hashtagsSeleccionados);
        newHastags[hashtagId] = newValue;

        this.setState({
            hashtags_seleccionados: newHastags,
            hashtags_cuenta: hastagCount
        })
    }

    renderCalificadores = ({item}) => {
        var tipo = item.pregunta_tipo;
        
        var initialValue = tipo == 'SI_NO' ? false : 1;
        var key = item.pregunta_id;
        var valor = this.state.respuestas[key] || initialValue;

        if(tipo == 'SI_NO'){
            valor = Boolean(valor);
        }
        
        if(tipo == 'UNO_CINCO'){
            valor = parseFloat(valor);
        }

        var handlerProp = (isBool = false) => {
            var key = isBool ? 'onPress' : 'onChange';
            var props = {}
            props[key] = (value) => {
                respuestas = Object.assign({}, this.state.respuestas)
                respuestas[item.pregunta_id] = value
                this.setState({ respuestas })
            }
            return props;
        }

        return (<CalificadorWidget 
            text={item.pregunta_texto}
            type={item.pregunta_tipo}
            quantifier={item.pregunta_cuantificador || 'calidad'}
            value={valor}
            {...handlerProp(tipo == 'SI_NO')}
            />)
    }
    
    componentDidUpdate(){
        if (this.props.side_effect == 'SHOW_SUCCESS') {
            this.props.getPerfilProfesor(
                this.props.estudiante_actual.estudiante_correo,
                this.props.perfil_profesor_actual.curso.curso_id,
                this.props.perfil_profesor_actual.profesor.profesor_id,
            );
            Actions.replace('success')
            return;
        }
        
        if (this.props.side_effect == 'SHOW_KITTY') {
            this.props.getPerfilProfesor(
                this.props.estudiante_actual.estudiante_correo,
                this.props.perfil_profesor_actual.curso.curso_id,
                this.props.perfil_profesor_actual.profesor.profesor_id,
            );
            Actions.replace('errorKitty')
            return;
        }
    }

    render() {
        if (this.props.loading) {
            return (<Loading message="Cargando..." />)
        }
        
        if (!this.props.tengo_preguntas) {
            return (<Loading message="Cargando preguntas..." />)
        }

        if (!this.props.tengo_hashtags) {
            return (<Loading message="Cargando hashtags..." />)
        }

        if (!this.props.tengo_carreras) {
            return (<Loading message="Cargando Carreras..." />)
        }

        if (!this.props.tengo_grados) {
            return (<Loading message="Cargando Grados..." />)
        }

        var grados_obtenidos = this.props.grados_obtenidos.map((grado) => ({
            label: grado.grado_descripcion,
            value: grado.grado_id,
        }))
        var carreras_disponibles = this.props.carreras_disponibles.map((carrera) => ({
            label: carrera.carrera_descripcion,
            value: carrera.carrera_id,
        }))

        var preguntas = this.props.preguntas;

        var hashtagsSeleccionados = this.state.hashtags_seleccionados
        
        var hashtags = this.props.hashtags;
        
        var etiquetas = hashtags.map((item, i) => {
            var hashtagId = item.hashtag_id;
            var selected = hashtagsSeleccionados[hashtagId] || false

            return (
                <Text
                    key={i}
                    style={{
                        backgroundColor: selected ? constants.COLOR_PRIMARY : 'white',
                        color: !selected ? constants.COLOR_PRIMARY : 'white',
                        borderColor: constants.COLOR_PRIMARY,
                        borderWidth: 1,
                        margin: 4,
                        paddingHorizontal: 10,
                        paddingVertical: 4,
                        borderRadius: 4,
                    }}
                    onPress={()  => this.setHashtag(item)}
                    >
                    {item.hashtag_hashtag}
                </Text>
            );
        })
        
        var preguntasUNO_CINCO = preguntas.filter((p) => p.pregunta_tipo == 'UNO_CINCO' );
        var preguntasSI_NO = preguntas.filter((p) => p.pregunta_tipo == 'SI_NO' );

        return (
            <ScrollView style={styles.mainView}
            contentContainerStyle={{
                paddingTop: 90,
            }}
            bounces={false}
            alwaysBounceVertical={false}>
                <View style={{
                    paddingHorizontal: 8
                }}>
                    <Text h4>Preguntas</Text>
                </View>
                

                <FlatList 
                    renderItem={this.renderCalificadores}
                    keyExtractor={(_, i) => i.toString()}
                    data={preguntasUNO_CINCO}
                />
                
                <CalificadorWidget 
                    text="¿Qué tan dificil es?"
                    type="UNO_CINCO"
                    value={this.state.dificultad}
                    quantifier="dificultad"
                    onChange={(value) => {
                        this.setState({dificultad: value})
                    }}
                    />
                
                <View style={{
                    paddingHorizontal: 8,
                    paddingVertical: 16
                }}>
                    <Text h4>Tags</Text>
                </View>

                <View style={{
                    flex: 1,
                    width: '100%',
                    flexWrap: 'wrap',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>
                    {etiquetas}
                </View>

                <FlatList 
                    renderItem={this.renderCalificadores}
                    keyExtractor={(_, i) => i.toString()}
                    data={preguntasSI_NO}
                />
                
                <CalificadorWidget 
                    text="¿Volverías a tomar el curso con este profesor?"
                    type="SI_NO"
                    value={this.state.tomar_denuevo}
                    onPress={(value) => {
                        this.setState({ tomar_denuevo: value})
                    }}
                    />
                
                <Input underlineColorAndroid="transparent"
                        placeholder="Escribe aquí tu comentario..."
                        inputStyle={styles.inputStyle}
                        containerStyle={{ alignSelf: 'center', marginBottom: 8 }}
                        inputContainerStyle={styles.inputContainerStyle}
                        autoCapitalize="none"
                        keyboardAppearance="light"
                        keyboardType="default"
                        autoCorrect={false}
                        multiline={true}
                        returnKeyType="next"
                        blurOnSubmit={true}
                        onChangeText={(text) => { this.setState({ 'comentario': text }) }}
                        value={this.state.comentario}
                    />

                <View style={{
                    paddingHorizontal: 16,
                }}>
                    <Text h6>¿Qué grado obtuviste en este curso?</Text>
                    <SelectInput 
                        options={grados_obtenidos}
                        placeholder="Selecciona el grado que obtuviste..."
                        onChange={(text) => { 
                            console.log('grado_obtenido', text)
                            this.setState({ grado_obtenido: text }) 
                        }}
                        value={this.state.grado_obtenido} 
                        />
                </View>

                <View style={{
                    paddingHorizontal: 16,
                }}>
                    <Text h6>¿Qué Carrera Cursas?</Text>
                    <SelectInput 
                        options={carreras_disponibles}
                        placeholder="Selecciona la carrera que cursas..."
                        onChange={(text) => { 
                            console.log('carrera_cursando', text)
                            this.setState({ carrera_cursando: text }) 
                        }}
                        value={this.state.carrera_cursando} 
                        />
                </View>


                <View style={{
                    alignSelf: 'center',
                    paddingHorizontal: 48,
                    paddingTop: 10,
                    paddingBottom: 20,
                    width: '100%'
                }}>
                    <GradientButton 
                        text="Finalizar Valoración"
                        textProps={{
                            h5: true,
                        }}
                        style={{
                            paddingHorizontal: 8,
                            paddingVertical: 10,
                            borderRadius: 8,
                            overflow: 'hidden'
                        }}
                        textStyle={{
                            color: 'white',
                            textAlign: 'center',
                        }}
                        onPress={() => this.doCalificar() }
                        colors={[constants.COLOR_PRIMARY, constants.COLOR_SECONDARY]}
                        start={{x: 0, y:0}}
                        end={{x: 1, y:0}}
                        />
                </View>

            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    mainView: {
        backgroundColor: constants.bgColor,
        flex: 1,
    },
    cargandoStyles: {
        backgroundColor: constants.bgColor, 
        height: '100%',
        alignContent: 'center',
        justifyContent: 'center',
        width: '100%'
    },
    redPatch: {
        height: 100,
        backgroundColor: constants.primaryBgColor,
    },
    mainTitle: {
        color: constants.primaryBgColor,
        alignSelf: 'center'
    },
    subtitle: { 
        marginBottom: 5, 
        fontSize: 20, 
        alignSelf: 'center'
    },
    profilePicHandle: {
        position: 'absolute',
        alignItems: 'center',
        width: '100%',
        top: StatusBar.currentHeight+10
    },
    profilePicBackground: {
        backgroundColor: constants.primaryLighterBgColor,
        width: 128, 
        height: 128,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 999, 
    },
    profilePicForeground: {
        backgroundColor: constants.bgColor,
        width: 120, 
        height: 120,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 999, 
    },
    tagBanner:{
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        padding: 8,
        backgroundColor: constants.primaryBgColor,
    },
    tagBannerImg:{
        width: 36,
        height: 36,
        marginRight: 8,
    },
    inputStyle: {
        paddingRight: 20,
        height: 120,
        // fontSize: 18,
    },
    inputContainerStyle: {
        marginVertical: 3,
        borderRadius: 7,
        backgroundColor: "white",
        paddingVertical: 10,
        borderWidth: 1,
    },
    formWrapper: {
        width: '100%',
    },
});

import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

import { getPreguntas, getHashtags, calificar, getPerfilProfesor,
    getGrados, getCarreras
} from '../../redux/actions';

const mapStateToProps = state => {
    return {
        estudiante_actual: state.estudiante_actual || {},
        perfil_profesor_actual: state.perfil_profesor_actual || {},
        preguntas: state.preguntas,
        hashtags: state.hashtags,
        loading: state.loading,
        tengo_preguntas: state.tengo_preguntas,
        tengo_hashtags: state.tengo_hashtags,
        tengo_grados: state.tengo_grados,
        tengo_carreras: state.tengo_carreras,
        side_effect: state.side_effect,
        grados_obtenidos: state.grados_obtenidos,
        carreras_disponibles: state.carreras_disponibles,
    };
};

const mapDispatchToProps = {
    getPreguntas,
    getHashtags,
    calificar,
    getPerfilProfesor,
    getGrados, 
    getCarreras,
};


export default connect(mapStateToProps, mapDispatchToProps)(Calificar);
