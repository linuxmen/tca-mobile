import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Alert,
    Dimensions,
} from 'react-native';

import * as constants from 'aula-upc/constants'
import {Icon, Input, Button} from 'react-native-elements';

import Store from 'aula-upc/redux/store';
import WebAPI from 'aula-upc/webapi';
import {List, fromJS} from 'immutable';
import BaseView from 'aula-upc/components/BaseView';

import Loading from 'aula-upc/components/Loading';

export default class NuevoHilo extends BaseView {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            asunto: '',
            mensaje: '',
        }
    }

    mensaje;

    static navigationOptions = {
        title: 'Enviar Mensaje Privado',
        headerStyle: {
            backgroundColor: constants.primaryBgColor
        },
        headerTitleStyle: {
            color: constants.bgColor,
        },        
        headerTintColor: constants.bgColor,
    }
    
    async _onPress(){
        this.setState({
            loading: true,
        });


        let comentarioActual = Store.get('comentario_actual');
        
        var response = await WebAPI.iniciarHilo(
            comentarioActual.get('inscripcion_id'), 
            this.state.asunto,
            this.state.mensaje
        )

        this.setState({
            asunto: '',
            mensaje: '',
        });

        Alert.alert(
            'Información',
            'Tu mensaje se ha enviado exitosamente!',
            [
                {text: 'OK', onPress: () => this.props.navigation.goBack()},
            ],
            { cancelable: false }
        )
            
    }

    onChangeSubject(){
        if(this.mensaje){
            this.mensaje.focus();
        }
    }

    render() {
        if (this.state.loading) {
            return (<Loading message="Enviando datos..."/>)
        }
        return (
            <View style={[styles.mainView, {paddingTop: 20}]}>
                <Input underlineColorAndroid="transparent" 
                    keyboardAppearance="light"
                    placeholder="Asunto"
                    onChangeText={(text) => { this.setState({'asunto': text})} }
                    label="Asunto"
                    containerStyle={{ 
                        alignSelf: 'center',
                        marginBottom: 20,
                    }}
                    labelStyle={{
                        color: constants.primaryBgColor,
                    }}
                    returnKeyType="next"
                    blurOnSubmit={true}
                    value={this.state.asunto}
                    onSubmitEditing={() => { this.onChangeSubject() }}
                    inputContainerStyle={{
                        borderBottomColor: 'transparent',
                        borderTopColor: constants.primaryBgColor,
                        borderTopWidth: 1
                    }} 
                    />
                <Input underlineColorAndroid="transparent" 
                    ref={(input) => (this.mensaje = input)}
                    onChangeText={(text) => { this.setState({'mensaje': text})} }
                    value={this.state.mensaje}
                    multiline={true}
                    numberOfLines={6}
                    // keyboardAppearance="light"
                    placeholder="Escribe aquí tu mensaje..."
                    inputStyle={{
                        textAlignVertical: 'top',
                        height: 120,
                    }}
                    label="Mensaje"
                    containerStyle={{
                        alignSelf: 'center',
                    }}
                    labelStyle={{
                        color: constants.primaryBgColor,
                    }}
                    inputContainerStyle={{
                        borderBottomWidth: 0,
                        borderTopColor: constants.primaryBgColor,
                        borderTopWidth: 1
                    }} 
                    />
                
                <View style={{width: 100, alignSelf: 'center', }}>
                    <Button 
                        title="Enviar" 
                        onPress={() => this._onPress()} 
                        buttonStyle={{
                            backgroundColor: constants.primaryBgColor
                        }}
                        />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    mainView: {
        backgroundColor: constants.bgColor,
        paddingTop: 4,
        height: '100%',
        width: '100%',
    },
});
