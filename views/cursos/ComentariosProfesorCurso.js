import React from 'react';
import {
    StyleSheet,
    FlatList,
    View,
    Button,
    Alert,
    Image,
    TouchableHighlight,
} from 'react-native';

import { fromJS, Map } from 'immutable';
import { createStackNavigator } from 'react-navigation';
import { LinearGradient } from 'expo'

import ComentarioListView from '../../components/ComentarioListView'
import Text from '../../components/BaseText'
import Loading from 'aula-upc/components/Loading'

import Style from '../../stylesheet'
import * as constants from '../../constants'
import { Button as RNEButton } from 'react-native-elements'

class ComentariosProfesorCurso extends React.Component {
    componentDidMount() {
        this.props.getComentarios(
            this.props.estudiante_actual.estudiante_correo,
            this.props.perfil_profesor_actual.curso_id,
            this.props.perfil_profesor_actual.profesor_id,
        );
    }

    startChat(item){
        this.props.createNewChat(this.props.estudiante_actual.estudiante_correo, item);
    }
    
    componentDidUpdate(){
        if(this.props.side_effect == 'START_CHAT'){
            Actions.viewChat()
        }
    }

    render() {
        if (this.props.loading) {
            return (<Loading message="Cargando comentarios..." />)
        }

        const comentarios = this.props.comentarios;

        return (
            <View style={styles.mainView}>
                <LinearGradient style={styles.gradientStyle}
                    colors={[constants.COLOR_TERTIARY, constants.COLOR_SECONDARY]}
                >
                    <Image
                        source={constants.ICONO_MEDALLA_OFF_GRANDE}
                        resizeMode="stretch"
                        style={{
                            width: 80,
                            height: 86,
                            position: 'absolute',
                            right: 16,
                            top: 24,
                        }} />

                    <Text style={styles.featuredText}>Comentarios</Text>
                </LinearGradient>

                <FlatList
                    data={comentarios}
                    ListEmptyComponent={() => (
                        <Text center>No hay comentarios...</Text>
                    )}
                    contentContainerStyle={{
                        paddingHorizontal: 8,
                    }}
                    keyExtractor={(item, key) => key.toString()}
                    renderItem={({ item }) => (
                        <ComentarioListView
                            inverted={true}
                            subtitle={"Alumno UPC"}
                            text={item.inscripcion_comentario}
                            rankValue={item.calificacion}
                            readonly={this.props.estudiante_actual.estudiante_id == item.estudiante_id}
                            onPress={() => {
                                this.startChat(item)
                            }}
                        />
                    )}
                />

            </View>
        );
    }

}


const styles = StyleSheet.create({
    mainView: {
        ...Style.StartMainView
    },
    gradientStyle: {
        ...Style.StartMainGradient
    },
    featuredText: {
        ...Style.StartFeaturedText
    }
});

import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

import { getComentarios, createNewChat } from '../../redux/actions';

const mapStateToProps = state => {
    return {
        comentarios: state.comentarios || [],
        estudiante_actual: state.estudiante_actual || {},
        perfil_profesor_actual: state.perfil_profesor_actual || {},
        loading: state.loading,
        side_effect: state.side_effect || null,
    };
};

const mapDispatchToProps = {
    getComentarios,
    createNewChat
};


export default connect(mapStateToProps, mapDispatchToProps)(ComentariosProfesorCurso);
