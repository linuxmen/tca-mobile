import React from 'react';
import {
    StyleSheet,
    FlatList,
    View,
    Button,
    Alert,
    Image,
    TouchableHighlight,
    Platform,
} from 'react-native';

import { fromJS, Map } from 'immutable';
import { createStackNavigator } from 'react-navigation';
import { LinearGradient } from 'expo'

import ProfesorItem from '../../components/ProfesorItem'
import Text from '../../components/BaseText'
import Loading from 'aula-upc/components/Loading'

import BaseViewWithBackButton from '../../components/BaseViewWithBackButton';
import Style from '../../stylesheet'
import * as constants from '../../constants'
import { Button as RNEButton } from 'react-native-elements'

import { get } from 'lodash'

class CursoSeleccionado extends React.Component {
    componentDidMount() {
        this.props.listProfesores(
            this.props.estudiante_actual.estudiante_correo, 
            this.props.curso_actual.curso_id
        );
    }
    render() {
        if (this.props.loading) {
            return (<Loading message="Cargando profesores..." />)
        }

        

        return (
            <View style={styles.mainView}>
                <LinearGradient style={styles.gradientStyle}
                    colors={[constants.COLOR_TERTIARY, constants.COLOR_SECONDARY]}
                    >
                    <Image
                        source={constants.ATOMO_LISTA_PROFESOR}
                        resizeMode="stretch"
                        style={{
                            width: 80,
                            height: 92,
                            position: 'absolute',
                            right: 16,
                            top: 32,
                        }} />

                    {false && (
                        <View style={{
                            position: 'absolute',
                            right: 16,
                            top: 30 + 24,
                            backgroundColor: 'white',
                            borderRadius: 48,
                            ...Platform.select({
                                android: {
                                    top: 30 + 32,
                                }
                            }),
                        }}>
                            <Image
                                source={constants.IMG_LOGO_UPC}
                                resizeMode="stretch"
                                style={{
                                    width: 64,
                                    height: 64,
                                }} />
                        </View>
                    )}

                    <Text style={styles.featuredText}>Busca cursos y profesores</Text>
                </LinearGradient>

                <FlatList
                    contentContainerStyle={{ flexGrow: 1, paddingHorizontal: 8, }}
                    data={this.props.profesores}
                    keyExtractor={(item, key) => key.toString()}
                    renderItem={({ item }) => {
                        var imgPerfil = get(item, 'profesor_pic');
                        if (imgPerfil) {
                            imgPerfil = {
                                uri: imgPerfil
                            }
                        } else {
                            imgPerfil = get(item, 'profesor_sexo') == 'm'
                                ? constants.DEFAULT_ICON_PROFESOR
                                : constants.DEFAULT_ICON_PROFESORA
                        }

                        console.log(imgPerfil);

                        return (
                            <ProfesorItem
                                title={item.nombre_completo}
                                subtitle={this.props.curso_actual.curso_nombre}
                                onPress={() => {
                                    this.props.setProfesorActual(item);
                                    this.props.setLoading(true);
                                    Actions.push('perfilProfesorCurso')
                                }}
                                ranking={item.calificacion}
                                imgSource={imgPerfil}
                            />
                        )}
                    }
                />

            </View>
        );
    }

}


const styles = StyleSheet.create({
    mainView: {
        ...Style.StartMainView
    },
    gradientStyle: {
        ...Style.StartMainGradient
    },
    featuredText: {
        ...Style.StartFeaturedText
    }
});

import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

import { listProfesores, setProfesorActual, setLoading } from '../../redux/actions';

const mapStateToProps = state => {
    return {
        estudiante_actual: state.estudiante_actual || {},
        curso_actual: state.curso_actual || {},
        profesores: state.profesores || [],
        loading: state.loading,
    };
};

const mapDispatchToProps = {
    listProfesores,
    setLoading,
    setProfesorActual,
};


export default connect(mapStateToProps, mapDispatchToProps)(CursoSeleccionado);
