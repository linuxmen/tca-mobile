import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    FlatList,
    Alert,
    Dimensions,
} from 'react-native';

import * as constants from 'aula-upc/constants'
import { Icon, Button, Text as TextRNE} from 'react-native-elements';
import PerfilMediaRow from 'aula-upc/components/PerfilMediaRow';

import Store from 'aula-upc/redux/store';
import {Map, fromJS} from 'immutable';

import BaseView from 'aula-upc/components/BaseView';
import Loading from 'aula-upc/components/Loading'
import Rating from 'aula-upc/components/Rating';

export default class EstadisticaProfesorCurso extends BaseView {
    static navigationOptions = {
        title: 'Estadísticas',
        headerStyle: {
            marginBottom: 0,
            backgroundColor: constants.primaryBgColor,
            shadowOpacity: 0,
            elevation: 0,
            borderBottomWidth: 0,
            borderBottomColor: 'transparent',
        },
        // header: null,
        headerTitleStyle: {
            color: constants.bgColor,
        },
        headerTintColor: constants.bgColor,
    }
    
    render() {
        var profesorActual = Store.get('current.profesor', Map());
        var cursoActual = Store.get('current.curso', Map());
        var claseCalificacion = Store.get('current.calificacion_clase', Map());
        
        var calificacionActual = parseFloat(Store.get('current.calificacion'))
        var tomarDenuevoActual = parseFloat(Store.get('current.tomar_denuevo'))
        var dificultadActual = parseFloat(Store.get('current.dificultad'))

        if(dificultadActual < 1 || Number.isNaN(dificultadActual)){
            dificultadActual = '---'
        }
        
        if(Number.isNaN(tomarDenuevoActual)){
            tomarDenuevoActual = '---'
        }else{
            tomarDenuevoActual *= 100;
            tomarDenuevoActual += '%';
        }

        if(calificacionActual < 1 || Number.isNaN(dificultadActual)){
            calificacionActual = 0
        }

        var hashtags = Store.get('current.hashtags', []);

        var etiquetas = hashtags.map((item, i) => {
            return (
                <Text
                    key={i}
                    style={{
                        backgroundColor: constants.primaryBgColor,
                        color: 'rgba(255,255,255,1)',
                        margin: 4,
                        paddingHorizontal: 12,
                        paddingVertical: 6,
                        fontSize: 18,
                        borderRadius: 4,
                    }}>
                    {item.get('hashtag_hashtag')}
                </Text>
            );
        })

        return (
            <ScrollView style={styles.mainView} 
                bounces={false}
                alwaysBounceVertical={false}
                >
                <View style={styles.redPatch}>
                </View>
                <View style={styles.whiteSection}>
                    <TextRNE style={styles.mainTitle} h3>{profesorActual.get('nombre_completo')}</TextRNE>
                    <Text style={styles.subtitle} h5>
                        {cursoActual.get('curso_nombre')}
                    </Text>

                    <View style={{
                        width: '100%',
                        alignSelf: 'center',
                        paddingHorizontal: 16,
                    }}>
                        <View style={{
                            borderTopColor: constants.primaryBgColor,
                            borderTopWidth: 1,
                            paddingVertical: 4,
                        }}></View>

                        <Rating
                            readonly
                            startingValue={calificacionActual}
                            />
                    </View>

                    <Image style={{
                            alignSelf: 'center',
                            width: 240,
                            height: 80
                        }}
                        resizeMode="contain"
                        source={{uri: claseCalificacion.get('icono')}}
                        />
                    
                    {/* Calificacion general */}
                    <View style={{
                        flex: 1,
                        flexDirection: 'row',
                        // justifyContent: 'space-evenly',
                        paddingHorizontal: 16,
                        marginTop: 6,
                    }}>
                        <View style={{
                            width: 40,
                            alignSelf: 'flex-start',
                            alignItems: 'center'
                        }}>
                            <Image
                                source={constants.CLASIFICACION_GENERAL_IMG}
                                style={{
                                    width: 24,
                                    height: 24,
                                }}
                            />
                        </View>
                        <View style={{
                            flexGrow: 1,
                        }}>
                            <TextRNE h4 style={{
                                textAlign: 'left',
                                // alignSelf: 'center',
                            }}>
                            Calificación General
                            </TextRNE>
                        </View>
                        <View style={{
                            width: 50,
                            borderWidth: 2,
                            borderColor: constants.primaryBgColor,
                            borderRadius: 40,
                            alignSelf: 'flex-end',
                            alignContent: 'flex-end',
                        }}>
                            <TextRNE h4 style={{
                                textAlign: 'center',
                                alignSelf: 'center',
                            }}>
                            {calificacionActual == 0 ? '---' : calificacionActual}
                            </TextRNE>
                        </View>
                    </View>
                    {/* dificultad */}
                    <View style={{
                        flex: 1,
                        flexDirection: 'row',
                        // justifyContent: 'space-evenly',
                        paddingHorizontal: 16,
                        marginTop: 6,
                    }}>
                        <View style={{
                            width: 40,
                            alignSelf: 'flex-start',
                            alignItems: 'center'
                        }}>
                            <Image
                                source={constants.DIFICULTAD_IMG}
                                style={{
                                    width: 24,
                                    height: 24,
                                }}
                            />
                        </View>
                        <View style={{
                            flexGrow: 1,
                        }}>
                            <TextRNE h4 style={{
                                textAlign: 'left',
                                // alignSelf: 'center',
                            }}>
                            Nivel de Dificultad
                            </TextRNE>
                        </View>
                        <View style={{
                            width: 50,
                            borderWidth: 2,
                            borderColor: constants.primaryBgColor,
                            borderRadius: 40,
                            alignSelf: 'flex-end',
                            alignContent: 'flex-end',
                        }}>
                            <TextRNE h4 style={{
                                textAlign: 'center',
                                alignSelf: 'center',
                            }}>
                            {dificultadActual}
                            </TextRNE>
                        </View>
                    </View>
                    {/* dificultad */}
                    <View style={{
                        flex: 1,
                        flexDirection: 'row',
                        // justifyContent: 'space-evenly',
                        paddingHorizontal: 16,
                        marginTop: 6,
                    }}>
                        <View style={{
                            width: 40,
                            alignSelf: 'flex-start',
                            alignItems: 'center'
                        }}>
                            <Image
                                source={constants.RECOMENDADO_IMG}
                                style={{
                                    width: 24,
                                    height: 24,
                                }}
                            />
                        </View>
                        <View style={{
                            flexGrow: 1,
                        }}>
                            <TextRNE h4 style={{
                                textAlign: 'left',
                                // alignSelf: 'center',
                            }}>
                            Lo Recomiendo
                            </TextRNE>
                        </View>
                        <View style={{
                            width: 75,
                            borderWidth: 2,
                            borderColor: constants.primaryBgColor,
                            borderRadius: 40,
                            alignSelf: 'flex-end',
                            alignContent: 'flex-end',
                        }}>
                            <TextRNE h4 style={{
                                textAlign: 'center',
                                alignSelf: 'center',
                            }}>
                            {tomarDenuevoActual}
                            </TextRNE>
                        </View>
                    </View>

                    <View style={{
                        flex: 1,         
                        width: '100%',
                        flexWrap: 'wrap',
                        flexDirection: 'row',
                        paddingVertical: 8,
                        paddingHorizontal: 8,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                        {etiquetas}
                    </View>

                </View>
                <View style={styles.profilePicHandle}>
                    <View style={styles.profilePicBackground}>
                        <View style={styles.profilePicForeground}>
                        <Image
                            resizeMode='contain'
                            source={constants.teacherPicture(profesorActual.get('profesor_sexo').toLowerCase() == 'f')}
                            style={styles.profilePic}
                            />
                    </View>
                    </View>
                </View>

            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    mainView: {
        backgroundColor: constants.bgColor,
    },
    redPatch: {
        height: 66,
        backgroundColor: constants.primaryBgColor,
    },
    whiteSection:{
        backgroundColor: constants.bgColor,
        paddingTop: 60,
    },
    mainTitle: {
        color: constants.primaryBgColor,
        alignSelf: 'center',
        textAlign: 'center',
    },
    subtitle: { 
        textAlign: 'center',
        marginBottom: 5,
        fontSize: 20, 
        alignSelf: 'center'
    },
    borderedSection: {
        borderTopColor: constants.primaryBgColor, 
        borderTopWidth: 1, 
        borderBottomColor: constants.primaryBgColor,
        borderBottomWidth: 1, 
        paddingHorizontal: 20,
        paddingVertical: 10,
        marginVertical: 10,
        width: '100%',
    },
    titleBorderedSection: {
        marginBottom: 5
    },
    editarBtnWrapper: {
        width: 140,
        alignSelf: 'center'
    },
    editarBtn: { 
        backgroundColor: constants.primaryBgColor,
        marginBottom: 10
    },
    profilePicHandle: {
        position: 'absolute',
        alignItems: 'center',
        width: '100%',
        // top: StatusBar.currentHeight
    },
    profilePicBackground: {
        backgroundColor: constants.primaryLighterBgColor,
        width: 128, 
        height: 128,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 999, 
    },
    profilePicForeground: {
        backgroundColor: constants.bgColor,
        width: 120, 
        height: 120,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 999, 
    },
    profilePicImage: {
        borderWidth: 5, 
        borderColor: 'white', 
        borderRadius: 999, 
        width: 120, 
        height: 120,
        overflow: 'hidden',
    },
    profilePic:{
        width: 120,
        height: 120,
        borderRadius: 999 
    }
});