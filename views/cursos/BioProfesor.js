import React from 'react';
import {
    StatusBar,
    StyleSheet,
    FlatList,
    View,
    ScrollView,
    Button,
    Alert,
    Image,
    TouchableHighlight,
    TouchableOpacity,
    Dimensions,
    Platform,
} from 'react-native';

import { LinearGradient} from 'expo'

import ProfesorItem from '../../components/ProfesorItem'
import Text from '../../components/BaseText'
import Rating from '../../components/Rating'
import Loading from 'aula-upc/components/Loading'

import Box from '../../components/BoxQuantifierImg';
import * as constants from '../../constants'
import Style, { normalize } from '../../stylesheet'
import { Button as RNEButton, Badge } from 'react-native-elements'

import LatestCommentLV from '../../components/LatestCommentLV';
import GradientButton from '../../components/GradientButton';
import { get } from 'lodash'

const DeviceDimensions = Dimensions.get('window')

class ProfesorCurso extends React.Component {
    render() {
        var { perfil_profesor_actual } = this.props;

        var imgPerfil = get(perfil_profesor_actual, 'profesor.profesor_pic');

        if (imgPerfil) {
            imgPerfil = {
                uri: imgPerfil
            }
        } else {
            var masculino = get(perfil_profesor_actual, 'profesor.profesor_sexo') == 'm';

            imgPerfil = masculino
                ? constants.DEFAULT_ICON_PROFESOR
                : constants.DEFAULT_ICON_PROFESORA
        }

        return (
            <ScrollView style={styles.mainView} contentContainerStyle={{
                alignItems: 'center',
            }}>
            <LinearGradient style={[styles.gradientStyle, {
              paddingBottom: 32,
            }]}
              colors={[constants.COLOR_TERTIARY, constants.COLOR_SECONDARY]}
            >
              <Image
                source={constants.ATOMO_BG_BIO}
                resizeMode="stretch"
                style={{
                  width: 80,
                  height: 96,
                  opacity: 0.2,
                  position: 'absolute',
                  right: 16,
                  top: 24,
                  ...Platform.select({
                    android: {
                      top: 32,
                    }
                  })
                }} />

              <Text style={styles.featuredText}>Hoja de Vida</Text>
            </LinearGradient>
            <StatusBar barStyle="dark-content" />
            <View style={{
                marginTop: -64,
                justifyContent: 'flex-start',
                alignItems: 'center',
                zIndex: 3,
            }}>
                <View style={{
                    borderRadius: 48,
                    backgroundColor: 'white',
                    overflow: 'hidden',
                    elevation: 5,
                }}>
                    <Image
                        source={imgPerfil}
                        resizeMode="stretch"
                        style={{
                            width: 96,
                            height: 96,
                        }}
                    />
                
                </View>
            </View>
            <View style={{
                height: 20,
                width: '100%'
            }}>
            </View>
            <View style={{
              paddingHorizontal: 16,
              alignItems: 'flex-start',
              width: '100%'
            }}>
                <Text h1 style={{
                  fontFamily: 'avenir-black',
                }}>Reseña</Text>
                
                <Text style={{
                  fontSize: normalize(19),
                  lineHeight: normalize(21),
                  marginBottom: 8,
                  textAlign: 'justify'
                }}>
                {get(perfil_profesor_actual, 'profesor.profesor_bio', 'No disponible aún...')}
                </Text>
              
              <Text h1 style={{
                fontFamily: 'avenir-black',
              }}>Educación</Text>

              <Text style={{
                fontSize: normalize(19),
                lineHeight: normalize(21),
                marginBottom: 8,
                textAlign: 'justify'
              }}>
                {get(perfil_profesor_actual, 'profesor.profesor_educacion', 'No disponible aún...')}
              </Text>

              <Text h1 style={{
                fontFamily: 'avenir-black',
              }}>Experiencia</Text>

              <Text style={{
                fontSize: normalize(19),
                lineHeight: normalize(21),
                marginBottom: 8,
                textAlign: 'justify'
              }}>
                {get(perfil_profesor_actual, 'profesor.profesor_experiencia', 'No disponible aún...')}
              </Text>

            </View>

          </ScrollView>
        );
    }

}


const styles = StyleSheet.create({
    mainView: {
        ...Style.StartMainView,
    },
    gradientStyle: {
        ...Style.StartMainGradient
    },
    featuredText: {
        ...Style.StartFeaturedText
    }
});

import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

const mapStateToProps = state => {
    return {
        state: state,
        estudiante_actual: state.estudiante_actual || {},
        curso_actual: state.curso_actual || {},
        profesor_actual: state.profesor_actual || [],
        perfil_profesor_actual: state.perfil_profesor_actual || {
            profesor: {},
            curso: {}
        },
    };
};

const mapDispatchToProps = {
};


export default connect(mapStateToProps, mapDispatchToProps)(ProfesorCurso);
