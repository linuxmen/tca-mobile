import React from 'react';
import {
    StatusBar,
    StyleSheet,
    FlatList,
    View,
    ScrollView,
    Button,
    Alert,
    Image,
    TouchableHighlight,
    TouchableOpacity,
    Dimensions
} from 'react-native';

import ProfesorItem from '../../components/ProfesorItem'
import Text from '../../components/BaseText'
import Rating from '../../components/Rating'
import Loading from 'aula-upc/components/Loading'

import Box from '../../components/BoxQuantifierImg';
import * as constants from '../../constants'
import Style from '../../stylesheet'
import { Button as RNEButton, Badge } from 'react-native-elements'

import LatestCommentLV from '../../components/LatestCommentLV';
import GradientButton from '../../components/GradientButton';
import { get } from 'lodash'

const DeviceDimensions = Dimensions.get('window')

class ProfesorCurso extends React.Component {
    componentDidMount() {
        this.props.getPerfilProfesor(
            this.props.estudiante_actual.estudiante_correo,
            this.props.curso_actual.curso_id,
            this.props.profesor_actual.profesor_id,
        );
    }
    render() {
        if (this.props.loading) {
            return (<Loading message="Cargando perfil del profesor..." />)
        }
        
        var { perfil_profesor_actual } = this.props;

        var imgPerfil = get(perfil_profesor_actual, 'profesor.profesor_pic');
        
        if (imgPerfil){
            imgPerfil = {
                uri: imgPerfil
            }
        }else {
            var masculino = get(perfil_profesor_actual, 'profesor.profesor_sexo') == 'm';
             
            imgPerfil = masculino 
                ? constants.DEFAULT_ICON_PROFESOR 
                : constants.DEFAULT_ICON_PROFESORA
        }

        return (
            <ScrollView style={styles.mainView} contentContainerStyle={{
                alignItems: 'center',
            }}>
                <StatusBar barStyle="dark-content" />
                <View style={{
                    top: 30,
                    position: 'absolute',
                    width: '100%',
                    zIndex: 1,
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                }}>
                    <Image
                        source={constants.ATOMO_BG}
                        style={{
                            width: 160,
                            height: 160,
                        }}
                        resizeMode="stretch"
                    />
                
                </View>
                <View style={{
                    marginTop: 30 + 34,
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                    zIndex: 3,
                }}>
                    <View style={{
                        borderRadius: 48,
                        backgroundColor: 'white',
                        overflow: 'hidden',
                        elevation: 5,
                    }}>
                        <Image
                            source={imgPerfil}
                            resizeMode="stretch"
                            style={{
                                width: 96,
                                height: 96,
                            }}
                        />
                    
                    </View>
                </View>

                <TouchableOpacity style={{
                    position: 'absolute',
                    top: 88,
                    zIndex: 4,
                    right: ((DeviceDimensions.width) / 2) - 72,
                    borderRadius: 48,
                    backgroundColor: 'white',
                    overflow: 'hidden',
                    elevation: 3,
                    width: 48,
                    height: 48,
                    padding: 8,
                }}
                    onPress={() => {
                        Actions.push('bioProfesor')
                    }}
                >
                    <Image
                        source={constants.DEFAULT_ICON_RESENIA_PROFE}
                        resizeMode="stretch"
                        style={{
                            width: 32,
                            height: 32,
                        }}
                    />
                </TouchableOpacity>

                <Text h4 style={{
                    marginVertical: 8
                }}>{get(perfil_profesor_actual, 'profesor.nombre_completo')}</Text>
                <Text h5>{get(perfil_profesor_actual, 'curso.curso_nombre')}</Text>
                
                <View style={{
                    flexDirection: 'row',
                    marginVertical: 8
                }}>
                    <Rating
                        imageSize={16}
                        startingValue={2.3}
                        ratingCount={5}
                        />
                    <Text style={{
                        marginLeft: 8
                    }}>25 Valoraciones</Text>
                </View>
                
                <View style={{
                    height: 32,
                    alignItems: 'center',
                    justifyContent: 'center',
                }}>
                    <FlatList
                        data={ get(perfil_profesor_actual, 'hashtags')}
                        horizontal
                        style={{
                            paddingHorizontal: 12,
                        }}
                        keyExtractor={(item, key) => key.toString()}
                        renderItem={({ item }) => (
                            <Badge containerStyle={{
                                backgroundColor: constants.COLOR_PRIMARY,
                                marginHorizontal: 4,
                            }}>
                                <Text style={{
                                    color: 'white'
                                }}>{item.hashtag_hashtag}</Text>
                            </Badge>
                        )}
                    />
                </View>

                <View style={{
                    paddingHorizontal: 8,
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                }}>
                    <Box
                        imgSource={constants.ICONO_COPA_COLOR}
                        ranking={(Math.round( get(perfil_profesor_actual, 'calificacion')) || 0)+"/5"}
                        text="General"
                    />
                    <Box
                        imgSource={constants.ICONO_COPA_COLOR}
                        ranking={(Math.round( get(perfil_profesor_actual, 'dificultad')) || 0)+"/5"}
                        text="Dificultad"
                    />
                    <Box
                        imgSource={constants.ICONO_COPA_COLOR}
                        ranking={(Math.round( get(perfil_profesor_actual, 'tomar_denuevo')) || 0)+"/5"}
                        text="Recomendación"
                    />
                </View>

                <View style={{
                    width: '100%',
                    paddingHorizontal: 16,
                    marginTop: 8,
                }}>
                    <Text h4 style={{
                        marginVertical: 8,
                    }}>Ultimos comentarios</Text>
                    
                    <FlatList
                        data={ get(perfil_profesor_actual, 'comentarios')}
                        ListEmptyComponent={() => (
                            <Text center>No hay comentarios...</Text>
                        )}
                        keyExtractor={(item, key) => key.toString()}
                        renderItem={({ item }) => (
                            <LatestCommentLV
                                text={item.inscripcion_comentario}
                                rankValue={item.calificacion}
                                onPress={() => {
                                    Actions.push('fullComments')
                                }}
                            />
                        )}
                    />
                    
                    { get(perfil_profesor_actual, 'comentarios.length') >= 3 && (
                        <TouchableOpacity activeOpacity={0.5} onPress={() => {
                            Actions.push('fullComments')
                        }}>
                            <Text h5 style={{
                                textAlign: 'center',
                                color: constants.COLOR_PRIMARY
                            }}>Ver más</Text>
                        </TouchableOpacity>
                    )}


                    { get(perfil_profesor_actual, 'puede_calificar') && (<GradientButton
                        colors={[constants.COLOR_PRIMARY, constants.COLOR_SECONDARY]}
                        text="¡Califica a tu Profesor!"
                        onPress={() => {
                            Actions.push('calificar')
                        }}
                        textStyle={{
                            color: 'white',
                            textAlign: 'center',
                        }}
                        textProps={{
                            h5: true,
                        }}
                        start={{
                            x: 0,
                            y: 0,
                        }}
                        end={{
                            x: 1,
                            y: 0,
                        }}
                        style={{
                            padding: 8,
                            borderRadius: 10,
                            overflow: 'hidden',
                            marginHorizontal: 16,
                            elevation: 3,
                            marginVertical: 8,
                        }}
                    />)}
                </View>

            </ScrollView>
        );
    }

}


const styles = StyleSheet.create({
    mainView: {
        ...Style.StartMainView,
    },
    gradientStyle: {
        // ...Style.BackButtonPadding
    },
    featuredText: {
        ...Style.StartFeaturedText
    }
});

import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

import { getPerfilProfesor } from '../../redux/actions';

const mapStateToProps = state => {
    return {
        state: state,
        estudiante_actual: state.estudiante_actual || {},
        curso_actual: state.curso_actual || {},
        profesor_actual: state.profesor_actual || [],
        perfil_profesor_actual: state.perfil_profesor_actual || {
            profesor: {},
            curso: {}
        },
        loading: state.loading,
    };
};

const mapDispatchToProps = {
    getPerfilProfesor
};


export default connect(mapStateToProps, mapDispatchToProps)(ProfesorCurso);
