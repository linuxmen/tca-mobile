import React from 'react';
import {
  StyleSheet,
  FlatList,
  View,
  Button,
  Alert,
  Image,
  TouchableHighlight,
} from 'react-native';

import Text from '../components/BaseText' 
import Loading from 'aula-upc/components/Loading'

import * as constants from '../constants'
import Style from '../stylesheet'

class Exito extends React.Component {
  componentDidMount() {
    this.props.clearSideEffects();
  }

  render() {
    return (
      <View style={styles.mainView}>
        <View style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'flex-end',
          paddingHorizontal: 8,
        }}>
          <Image source={constants.ICONO_MEDALLA_COLOR}
            resizeMode="contain"
            style={{
              width: 48,
              opacity: 0.5,
            }}
          />
          <Text h2 style={{
            // fontFamily: 'avenir-black'
          }}>¡OOPS!</Text>
          />
          <Text style={{
            textAlign: 'center',
          }} h5>
            Estamos presentando problemas técnicos. Te pedimos que disfrutes de este gatito hasta resolver los inconvenientes.
          </Text>
        </View>
        <View style={{
          flex: 1,
          justifyContent: 'flex-end',
          width: '100%',
          alignItems: 'flex-end',
        }}>
          <Image source={constants.IMG_GATO}
            resizeMode="stretch"
            style={{
              width: 128,
              height: 150,
            }}
          />
        </View>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  mainView: {
    ...Style.StartMainView,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

import { connect } from 'react-redux'

import { clearSideEffects } from '../redux/actions';

const mapStateToProps = state => {
  return {
  };
};

const mapDispatchToProps = {
  clearSideEffects
};


export default connect(mapStateToProps, mapDispatchToProps)(Exito);
