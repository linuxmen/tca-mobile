import React from 'react';
import {ListItem} from 'react-native-elements';
import * as constants from 'aula-upc/constants'

export default class PerfilMediaRow extends React.Component {
    render() {
        var defaultIcon = { 
            name: "intersex",
            color: constants.primaryBgColor,
            type: "font-awesome",
            size: 24,
        };
        var text = this.props.text;
        var icon = Object.assign({}, defaultIcon, this.props.icon || {});

        return (
            <ListItem
                leftIcon={icon}
                containerStyle={{
                    padding: 2
                }}
                title={text}
            />
        );
    }
}
