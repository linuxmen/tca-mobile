import React from 'react';
import {Rating as BaseRating} from 'react-native-elements';
import * as constants from 'aula-upc/constants';

export default class Rating extends React.Component {
    render() {
        return (
            <BaseRating 
                ratingImage={constants.ICON_STAR}
                imageSize={60}
                ratingCount={5}
                ratingColor={constants.COLOR_PRIMARY}
                type="custom"
                {...this.props}
                 />
        );
    }
}

