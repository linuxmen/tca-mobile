import React from 'react';
import Text from './BaseText';

import { TouchableOpacity } from 'react-native';

import { LinearGradient } from 'expo';

export default class GradientButton extends React.Component {
    render() {
        var {
            colors, 
            style, 
            start, 
            end, 
            textStyle, 
            text, 
            textProps,
            gradientProps,
            onPress,
            activeOpacity = 0.5,
            ...remainingProps
        } = this.props;

        return (
            <TouchableOpacity onPress={onPress} activeOpacity={activeOpacity} >
                <LinearGradient
                    colors={colors}
                    start={start}
                    end={end}
                    style={style}
                    {...gradientProps}
                    >
                    <Text
                        style={textStyle}
                        {...textProps}
                        >
                        {text}
                    </Text>
                </LinearGradient>
            </TouchableOpacity>
        );
    }
}