import React from 'react';
import { Input } from 'react-native-elements';

class BaseInput extends React.Component {
    
    render() {
      const { inputStyle, ...remainingProps} = this.props;
      var textSizeStyle = {
      }

      if(this.props.fontLoaded){
          textSizeStyle.fontFamily = 'avenir-book';
      }
      return (
        <Input inputStyle={[textSizeStyle, inputStyle]}
          {...remainingProps}
          />
      );
    }
}

import { connect } from 'react-redux';

const mapStateToProps = state => {
    return {
        fontLoaded: state.fontLoaded,
    };
};

export default connect(mapStateToProps)(BaseInput);
