import React from 'react';
import { Alert, View, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { ListItem, Slider } from 'react-native-elements';
import * as constants from '../constants'
import Style from '../stylesheet'
import Text from './BaseText'

export default class CursoItem extends React.Component {
  render() {
    const {
      onPress,
      imgSource = constants.DEFAULT_ICON_CURSO,
      title,
      subtitle,
      titleStyle = {},
      imgStyle = {},
      subtitleStyle = {},
      style = {},
      score,
      scoreStyle = {},
      Component = onPress ? TouchableOpacity : View
    } = this.props;

    return (
      <Component style={{
        flex: 1,
        flexDirection: 'row',
        marginVertical: 8,
      }}>
        <View style={{ width: 64, justifyContent: 'center', alignItems: 'center' }}>
          <Image
            source={imgSource}
            resizeMode='stretch'
            style={imgStyle}
            />
        </View>
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <Text h4 style={[{
            fontFamily: 'avenir-black',
          }, titleStyle]}>{title}</Text>
          <Text>{subtitle}</Text>
        </View>
        <View style={{ width: 64, justifyContent: 'center', alignItems: 'center' }}>
          <Text h1 style={[{
            fontFamily: 'avenir-black',
            color: constants.COLOR_PRIMARY,
          }, scoreStyle]}>{score}</Text>
        </View>
      </Component>
      // <ListItem
      //   leftAvatar={{
      //     source: imgSource,
      //     size: 100,
      //     rounded: true,
      //     overlayContainerStyle:{
      //       backgroundColor: 'transparent',
      //     }
      //   }}
      //   title={title}
      //   subtitle={subtitle}
      //   subtitleStyle={subtitleStyle}
      //   titleStyle={titleStyle}
      //   />
    );
  }
}

const styles = StyleSheet.create({
  highlight: {
    flex: 1,
    paddingHorizontal: 8,
    paddingVertical: 8,
    maxWidth: '33.33333%',
  },
  item: {
    ...Style.StartMainGridItem,
  }
})