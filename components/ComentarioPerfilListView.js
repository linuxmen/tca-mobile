import React from 'react';
import {
    StyleSheet,
    Text,
    View,
} from 'react-native';

import * as constants from '../constants'
import {Icon, ListItem} from 'react-native-elements';
import Rating from 'aula-upc/components/Rating';
import isObject from 'lodash/isObject'
import isUndefined from 'lodash/isUndefined'

export default class ComentarioPerfilListView extends React.Component {
    render() {
        var {
            commentCount,
            rankValue,
            subtitle,
            rounded,
            rightIcon,
            ...remainingProps
        } = this.props;

        var icon = 'torso'
        
        if(this.props.female){
            icon = 'torso-female'
        }

        var subtitleElement = (<View style={styles.subtitleWrapper}>
            {subtitle && (
                <Text>{subtitle}</Text>
            )}
            <View style={styles.splitWrapper}>
                {rankValue && (
                    <View style={styles.leftSplit}>
                        <Rating
                            readonly
                            startingValue={rankValue}
                            />
                    </View>
                )}
                { commentCount && (<View style={styles.rightSplit}>
                    <Text style={styles.commentCount}>{commentCount}</Text>
                    <Icon name="comment" color={constants.primaryBgColor} />
                </View>)}
                
            </View>
        </View>);

        var rounded = rounded;
        if(!rounded && rounded !== false){
            rounded = true;
        }

        var defaultRightIcon = {
            name: 'chevron-right',
            color: constants.primaryBgColor,
            size: 32,
        };
        
        if(rightIcon || isUndefined(rightIcon)){
            rightIcon = Object.assign({}, defaultRightIcon, isObject(rightIcon) ? rightIcon : {})
        }

        return (
            <ListItem title={this.props.title} 
            onPress={this.props.onPress}
            leftAvatar={{ 
                rounded,
                icon: {
                    type: 'foundation',
                    name: icon
                }
            }}
            bottomDivider={true}
            rightIcon={rightIcon}
            subtitle={subtitleElement}
            {...remainingProps}
            />            
        );
    }
}

const styles = StyleSheet.create({
    subtitleWrapper: { alignItems: 'flex-start', width: '100%' },
    splitWrapper: { 
        flexDirection: 'row', 
        justifyContent: 'space-around'
    },
    leftSplit: { width:'50%', flexDirection: 'row' },
    rightSplit: { width:'50%', flexDirection: 'row-reverse' },
    commentCount: { alignSelf: 'flex-end', fontSize: 16 },
});
