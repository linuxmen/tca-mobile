import React from 'react';
import {View, Image} from 'react-native';
import * as constants from '../constants'

export default class BaseViewWithBackButton extends React.Component {
    static navigationOptions = {
        title: '',
        headerBackTitle: null,
        headerTruncatedBackTitle: null,
        headerTransparent: true,
        headerMode: 'float',
        headerStyle: {
            elevation: 0,
            borderBottomWidth: 0,
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0
        },
        headerBackImage: () => (
            <View style={{
                backgroundColor: 'white',
                borderRadius: 24,
                elevation: 5,
                padding: 8,
                marginLeft: 16,
            }}>
                <Image
                    source={constants.ICON_BACK}
                    resizeMode="stretch"
                    style={{
                        height: 20,
                        width: 20,
                    }}
                />
            </View>
        ),
    }
}