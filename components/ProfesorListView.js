import React from 'react';
import {
    StyleSheet,
    Text,
    View,
} from 'react-native';

import * as constants from '../constants'
import {Icon, ListItem} from 'react-native-elements';
import Rating from 'aula-upc/components/Rating';
import {isNumber} from 'lodash';

export default class ProfesorListView extends React.Component {
    render() {
        
        var {
            commentCount,
            rankValue,
            female,
            ...remainingProps
        } = this.props;

        var hasCommentCount = isNumber(commentCount);

        var icon = constants.teacherPicture(female);

        var subtitle = (<View style={styles.subtitleWrapper}>
            {this.props.subtitle && (
                <Text>{this.props.subtitle}</Text>
            )}
            <View style={styles.splitWrapper}>
                <View style={styles.leftSplit}>
                    <Rating
                        readonly
                        imageSize={20}
                        startingValue={rankValue}
                        />
                </View>
                { hasCommentCount && (<View style={styles.rightSplit}>
                    <Text style={styles.commentCount}>{commentCount}</Text>
                    <Icon name="comment" color={constants.primaryBgColor} containerStyle={{
                        marginRight: 3
                    }} />
                </View>)}
                
            </View>
        </View>);


        return (
            <ListItem title={this.props.title} 
            leftAvatar={{
                source: icon
            }}
            bottomDivider={true}
            rightIcon={{
                name: 'chevron-right',
                color: constants.primaryBgColor,
                size: 32,
            }}
            subtitle={subtitle}
            {...remainingProps}
            />            
        );
    }
}

const styles = StyleSheet.create({
    subtitleWrapper: { alignItems: 'flex-start', width: '100%' },
    splitWrapper: { 
        flexDirection: 'row', 
        justifyContent: 'space-around'
    },
    leftSplit: { width:'50%', flexDirection: 'row' },
    rightSplit: { width:'50%', flexDirection: 'row-reverse' },
    commentCount: { alignSelf: 'flex-end', fontSize: 16, },
});
