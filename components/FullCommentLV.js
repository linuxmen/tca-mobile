import React from 'react';
import { View, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { ListItem, Slider, Rating } from 'react-native-elements';
import * as constants from '../constants'
import Style from '../stylesheet'
import Text from './BaseText'

import ColoredRating from './ColoredRating';

export default class FullCommentLV extends React.Component {
    render() {
        const {
            imgSource = constants.DEFAULT_ICON_PROFESOR,
            text,
            comment,
            category,
            rankValue, 
            imgStyle = {},
            imgContainerStyle={},
            onPressLeftIcon,
        } = this.props;

        return (<View style={{
            marginBottom: 16,
        }}>
            <View style={{
                flexDirection: 'row',
                alignItems: 'flex-end',
                marginBottom: 4,
            }}>
                <View style={{
                    borderRadius: 30,
                    overflow: 'hidden',
                    width: 48,
                    height: 48,
                    marginRight: 16,
                    ...imgContainerStyle
                }}>
                    <Image
                        source={imgSource}
                        style={{
                            width: 48,
                            height: 48,
                            ...imgStyle
                        }}
                        resizeMode="stretch"
                    />
                </View>
                <View style={{
                    flex: 1,
                }}>
                    <Text>{text}</Text>
                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                    }}>
                        <ColoredRating rankValue={rankValue} />
                        <Text>{category}</Text>
                    </View>
                </View>
            </View>
            
            <View style={{
                flexDirection: 'row-reverse',
                flex: 1,
                }}>
                <TouchableOpacity onPress={onPressLeftIcon} activeOpacity={0.5} 
                    style={{
                        width: 32,
                        height: 32,
                        flexBasis: 32,
                        overflow: 'hidden',
                        borderRadius: 16,
                    }}>
                    <Image
                        source={constants.DEFAULT_ICON_PROFESOR}
                        style={{
                            width: 32,
                            height: 32,
                        }}
                        resizeMode="stretch"
                        />
                </TouchableOpacity>
                <View style={{
                    flex: 1,
                }}>
                    <Text>{comment}</Text>
                </View>
            </View>

        </View>
        );
    }
}

const styles = StyleSheet.create({
})