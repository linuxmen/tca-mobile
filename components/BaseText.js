import React from 'react';
import { Text, StyleSheet, Platform } from 'react-native';
import * as constants from 'aula-upc/constants'
import Style from '../stylesheet'

class BaseText extends React.Component {
    
    render() {
        const {
            style = {},
            children,
            h1,
            h2,
            h3,
            h4,
            h5,
            h6,
            small,
            center,
            ...remainingProps
        } = this.props;
        
        var textSizeStyle = {
        };

        var ffstyle = {};

        if(this.props.fontLoaded){
            ffstyle.fontFamily = 'avenir-book';
        }
        
        if (h1) {
            textSizeStyle = Style.TextH1
        }
        if (h2) {
            textSizeStyle = Style.TextH2
        }
        if (h3) {
            textSizeStyle = Style.TextH3
        }
        if (h4) {
            textSizeStyle = Style.TextH4
        }
        if (h5) {
            textSizeStyle = Style.TextH5
        }
        if (h6) {
            textSizeStyle = Style.TextH6
        }
        if (small) {
            textSizeStyle = Style.TextSmall
        }
        if (center) {
            textSizeStyle = Object.assign(textSizeStyle, {
                textAlign: 'center'
            })
        }

        if (!this.props.fontLoaded) {
            style.fontFamily = Platform.OS == 'ios' ? 'Helvetica' : 'Roboto'
        }

        return (
            <Text style={[styles.baseText, textSizeStyle, ffstyle, style]}
            {...remainingProps}
            >
                {children}
            </Text>
        );
    }
}

const styles = StyleSheet.create({
    baseText: {
        ...Style.BaseText
    },
})

import { connect } from 'react-redux';

const mapStateToProps = state => {
    return {
        fontLoaded: state.fontLoaded,
    };
};

export default connect(mapStateToProps)(BaseText);
