import React from 'react';
import { View, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { ListItem, Slider, Rating } from 'react-native-elements';
import * as constants from '../constants'
import Style from '../stylesheet'
import Text from './BaseText'

import ColoredRating from './ColoredRating';

export default class LatestCommentLV extends React.Component {
    render() {
        const {
            imgSource = constants.DEFAULT_ICON_ALUMNO,
            text,
            rankValue, 
            imgStyle = {},
            onPress,
        } = this.props;

        var categories = ['Pésimo', 'Puede mejorar', 'Regular', 'Bueno', '¡Muy Bueno!']
        var category = categories[rankValue % 5];

        return (<TouchableOpacity onPress={onPress} style={{
            flexDirection: 'row',
            alignItems: 'flex-end',
            marginBottom: 8,
        }}>
            <View style={{
                borderRadius: 30,
                overflow: 'hidden',
                width: 48,
                height: 48,
                marginRight: 16,
            }}>
                <Image
                    source={imgSource}
                    style={{
                        width: 48,
                        height: 48,
                        ...imgStyle
                    }}
                    resizeMode="stretch"
                />
            </View>
            <View style={{
                flex: 1,
            }}>
                <Text>{text}</Text>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                }}>
                    <ColoredRating rankValue={rankValue} />
                    <Text style={{
                        // fontFamily: 'avenir-black',
                        letterSpacing: 0.4,
                    }}>{category}</Text>
                </View>
            </View>
        </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
})