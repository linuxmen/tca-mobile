import React from 'react';
import { View, StyleSheet, TouchableHighlight, Image } from 'react-native';
import { ListItem, Slider, Rating } from 'react-native-elements';
import * as constants from '../constants'
import Style from '../stylesheet'
import Text from './BaseText'


export default class ColoredRating extends React.Component {
    render() {
        var {
            rankValue,
            colors = [],
        } = this.props;

        var defaultColors = [
            '#00CB5D', // muy buena
            '#4CF4D6', // buena
            '#4CB0F4', // regular
            '#FC6C03', // mala
            '#E80606', // peor
        ];

        rankValue = 5 - Math.min(Math.round(rankValue), 5);
       

        for( var i in defaultColors ){
            if (colors[i]) {
                defaultColors[i] = colors[i];
            }
        }

        return (
            <View style={{
                flexDirection: 'row',
                alignItems: 'center',
            }}>
                {defaultColors.map((e, k) => {
                    var prop = 'marginHorizontal';
                    if (k == 0){
                        prop = 'marginRight'
                    }
                    if ( k == 4){
                        prop = 'marginLeft'
                    }
                    var extraStyle = {
                        opacity: 0.2
                    };
                    extraStyle[prop] = 6;

                    if (rankValue == k) {
                        extraStyle.opacity = 1
                        extraStyle.elevation = 5
                    }

                    return (<View key={k} style={{
                        width: 10,
                        height: 10,
                        backgroundColor: e,
                        borderRadius: 10,
                        ...extraStyle,
                    }}></View>);
                })}
            </View>
        );
    }
}

const styles = StyleSheet.create({
})