import React from 'react';
import {View} from 'react-native';
import {ListItem, Slider, ButtonGroup} from 'react-native-elements';
import * as constants from '../constants'
import Text from './BaseText'
export default class CalificadorWidget extends React.Component {
    SWITCH = 'SI_NO'
    SLIDER = 'UNO_CINCO'

    constructor(props){
        super(props);
    }

    render() {
        var {
            text,
            type,
            value,
            onChange,
            onPress,
            quantifier = "calidad",
            ...remainingProps
        } = this.props;

        var control = null;
        if (type == this.SWITCH){
            buttons = ['Si', 'No'];

            remainingProps['buttonGroup'] = {
                onPress: (value) => {
                    return onPress(value == 0)
                },
                selectedIndex: value ? 0 : 1,
                buttons: buttons,
                innerBorderStyle: {
                    color: constants.COLOR_PRIMARY
                },
                containerStyle: {
                    borderColor: constants.COLOR_PRIMARY,
                    width: 96,
                    height: 32,
                    flex: 0
                },
                selectedButtonStyle: {
                    backgroundColor: constants.COLOR_SECONDARY
                },    
            }
        }else if (type == this.SLIDER){

            quantifier = quantifier.toUpperCase()

            var words = {
                low: 'Regular',
                mid: 'Bueno',
                hi: 'Excelente',
            }
            if (quantifier == 'CANTIDAD'){
                words.low = 'Poco';
                words.mid = 'Regular';
                words.hi = 'Bastante';
            }

            if (quantifier == 'FRECUENCIA_INV'){
                words.low = 'Si';
                words.mid = 'A veces';
                words.hi = 'Nunca';
            }

            if (quantifier == 'FRECUENCIA'){
                words.low = 'Nunca';
                words.mid = 'A veces';
                words.hi = 'Si';
            }

            if (quantifier == 'DIFICULTAD'){
                words.low = 'Fácil';
                words.mid = 'Regular';
                words.hi = 'Difícil';
            }

            control = (<View style={{
                flex: 1,
                width: '100%',
            }}>
                <Slider 
                    animateTransitions
                    value={value}
                    minimumValue={1}
                    maximumValue={5}
                    onValueChange={onChange}
                    step={1}
                    trackStyle={{
                        backgroundColor: constants.COLOR_PRIMARY
                    }}

                    thumbTintColor={constants.COLOR_SECONDARY}
                    minimumTrackTintColor={constants.COLOR_SECONDARY}
                    />
                <View style={{
                    width: '100%',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    flexDirection: 'row'
                }}>
                    <Text style={{
                        width: '33.333%',
                        textAlign: 'left',
                    }}>{words.low}</Text>
                    <Text style={{
                        width: '33.333%',
                        textAlign: 'center',
                    }}>{words.mid}</Text>
                    <Text style={{
                        width: '33.333%',
                        textAlign: 'right',
                    }}>{words.hi}</Text>
                </View>
            </View>);
            
            remainingProps['subtitle'] = control
        }
        
        return (
            <ListItem
                title={(<Text>{text}</Text>)}
                {...remainingProps}
            />
        );
    }
}
