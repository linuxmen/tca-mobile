import React, { Component } from 'react';
import { View, Text, FlatList, StyleSheet } from 'react-native';
import { connect } from 'react-redux';

import { listRepos } from '../redux/actions';

class RepoList extends Component {
    componentDidMount() {
        this.props.listRepos('relferreira');
    }
    renderItem = ({ item }) => (
        <View style={styles.item}>
            <Text>{item.name}</Text>
        </View>
    );
    render() {

        const { repos, loading } = this.props;
        
        if (loading) {
            return (<Text>Loading...</Text>)
        }

        return (
            <FlatList
                styles={styles.container}
                data={repos}
                keyExtractor={(item, index) => index.toString()}
                renderItem={this.renderItem}
            />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    item: {
        padding: 16,
        borderBottomWidth: 1,
        borderBottomColor: '#ccc'
    }
});

const mapStateToProps = state => {
    let storedRepositories = state.repos.map(repo => ({ key: repo.id, ...repo }));
    return {
        repos: storedRepositories,
        loading: state.loading,
    };
};

const mapDispatchToProps = {
    listRepos
};

export default connect(mapStateToProps, mapDispatchToProps)(RepoList);