import React from 'react';
import { Platform,  View, StyleSheet, TouchableHighlight, Image } from 'react-native';
import { ListItem, Slider } from 'react-native-elements';
import * as constants from '../constants'
import Style from '../stylesheet'
import Text from './BaseText'
import Rating from './Rating'
import {isNumber} from 'lodash'

import {normalize} from '../stylesheet'

export default class CursoItem extends React.Component {
    render() {
        const {
            onPress,
            title,
            subtitle,
            titleProps,
            titleStyle,
            subtitleProps,
            subtitleStyle,
            ranking,
            rightText,
            rightTextStyle = {},
            rightBgColor = constants.COLOR_CHEVRON, 
            imgSource = constants.DEFAULT_ICON_PROFESOR,
            ...remainingProps
        } = this.props;
        
        return (
            <View>
                <ListItem
                    onPress={onPress}
                    containerStyle={{
                        paddingVertical: 8,
                        paddingHorizontal: 0,
                    }}
                    leftElement={
                        <View style={{
                            borderRadius: 64,
                            overflow: 'hidden',
                        }}>
                            <Image
                                source={imgSource}
                                style={{
                                    width: 48,
                                    height: 48,
                                }}
                                resizeMode="stretch"
                            />
                        </View>
                    }
                    rightElement={
                        <View
                            style={{
                                width: 16,
                                height: 16,
                                backgroundColor: rightBgColor,
                                
                                borderRadius: 16,
                                justifyContent: 'center',
                                alignItems: 'center',
                                ...Platform.select({
                                    android:{
                                        paddingTop: 2,
                                    },
                                    ios: {
                                        paddingTop: 4,
                                    }
                                })
                            }}
                        >
                            <Text style={[{
                                fontSize: normalize(9),
                                lineHeight: normalize(9),
                                textAlign: 'center',
                            }, rightTextStyle]}>{rightText}</Text>
                        </View>
                    }
                    title={<Text
                        style={titleStyle}
                        {...titleProps}>
                        {title}
                    </Text>
                    }
                    subtitle={
                        <View>
                            <Text
                                style={subtitleStyle}
                                {...subtitleProps}
                            >
                                {subtitle}
                            </Text>
                            {isNumber(ranking) && <Rating
                                style={{
                                    alignSelf: 'flex-start'
                                }}
                                startingValue={ranking}
                                readonly
                                imageSize={16}
                            />}
                        </View>
                    }

                    {...remainingProps}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    highlight: {
        flex: 1,
        paddingHorizontal: 8,
        paddingVertical: 8,
    },
    item: {
        ...Style.StartMainGridItem,
    }
})