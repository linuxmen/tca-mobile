import React from 'react';
import { Button } from 'react-native-elements';
import { Platform } from 'react-native';

class BaseButton extends React.Component {
    
    render() {
      const { titleStyle = {}, ...remainingProps} = this.props;
      var textSizeStyle = {
      }

      if(this.props.fontLoaded){
        textSizeStyle.fontFamily = 'avenir-black';
        if (Platform.OS == "android")
          textSizeStyle.fontWeight = '400';
      }
      return (
        <Button titleStyle={[textSizeStyle, titleStyle]}
          {...remainingProps}
          />
      );
    }
}

import { connect } from 'react-redux';

const mapStateToProps = state => {
    return {
        fontLoaded: state.fontLoaded,
    };
};

export default connect(mapStateToProps)(BaseButton);
