import React from 'react';
import { View, StyleSheet, TouchableHighlight, Image } from 'react-native';
import { ListItem, Slider, Rating } from 'react-native-elements';
import * as constants from '../constants'
import Style from '../stylesheet'
import Text from './BaseText'


export default class BoxQuantifierImg extends React.Component {
    render() {
        const {
            imgSource,
            text,
            ranking,
            containerStyle={}
        } = this.props;

        return (
            <View style={{
                flexDirection: 'column',
                padding: 8,
                ...containerStyle
            }}>
                <View style={{
                    elevation: 5,
                    shadowColor: 'black',
                    shadowOffset: {
                        height: 3,
                    },
                    shadowRadius: 5,
                    shadowOpacity: .5,
                    borderRadius: 12,
                    paddingHorizontal: 8,
                    paddingVertical: 12,
                    backgroundColor: 'white',
                }}>
                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}>
                        <Image
                            source={imgSource}
                            resizeMode="contain"
                            style={{
                                width: 32,
                                height: 32,
                                marginRight: 6,
                                marginBottom: 4,
                            }}
                        />
                        <Text style={{
                            color: constants.COLOR_PRIMARY
                        }} h3>{ranking}</Text>
                    </View>
                    <Text style={{
                        textAlign: 'center',
                        letterSpacing: 0.4
                    }} small>{text}</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
})