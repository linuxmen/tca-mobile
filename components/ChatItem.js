import React from 'react';
import { View, StyleSheet, TouchableHighlight, Image } from 'react-native';
import { ListItem, Slider } from 'react-native-elements';
import * as constants from '../constants'
import Style from '../stylesheet'
import Text from './BaseText'
import Rating from './Rating'
import {isNumber} from 'lodash'
import {LinearGradient} from 'expo'



export default class CursoItem extends React.Component {
    render() {
        const {
            message,
            right,
            ...remainingProps
        } = this.props;
        
        var Component = right ? LinearGradient : View;

        var componentProps = {
            style: {
                paddingHorizontal: 8,
                paddingVertical: 16,
                marginBottom: 10,
                borderBottomLeftRadius: 12,
                borderBottomRightRadius: 12,
                overflow: 'hidden',
            },
        };

        if(right){
            componentProps = {
                ...componentProps,
                colors: [constants.COLOR_SECONDARY, constants.COLOR_TERTIARY],
                start: { x: 0, y: 0 },
                end: { x: 0, y: 1 },
                style: {
                    ...componentProps.style,
                    borderTopLeftRadius: 12,
                }
            }
        }else{
            componentProps = {
                ...componentProps,
                style: {
                    ...componentProps.style,
                    backgroundColor: constants.COLOR_PRIMARY,
                    borderTopRightRadius: 12,
                }
            }
        }

        return (
            <Component {...componentProps}>
                <Text style={{
                    textAlign: right ? 'right' : 'left',
                    color: 'white',
                }}>{message}</Text>
            </Component>
        );
    }
}

const styles = StyleSheet.create({
    highlight: {
        flex: 1,
        paddingHorizontal: 8,
        paddingVertical: 8,
    },
    item: {
        ...Style.StartMainGridItem,
    }
})