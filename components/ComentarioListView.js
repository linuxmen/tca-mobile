import React from 'react';
import { View, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { ListItem, Slider, Rating } from 'react-native-elements';
import * as constants from '../constants'
import Style from '../stylesheet'
import Text from './BaseText'

import ColoredRating from './ColoredRating';

export default class ComentarioListView extends React.Component {
    render() {
        const {
            imgSource = constants.DEFAULT_ICON_ALUMNO,
            rightImgSource = constants.ICONO_GLOBO_COLOR,
            text,
            readonly = false,
            rankValue, 
            subtitle,
            onPress,
            inverted = false,
            imgStyle = {},
        } = this.props;

        var categories = ['Pésimo', 'Puede mejorar', 'Regular', 'Bueno', '¡Muy Bueno!']
        var category = categories[rankValue % 5];

        return (<View style={{
            flex: 1,
            marginBottom: 16,
        }}>
            <View style={{
                flexDirection: 'row',
                marginBottom: 8,
            }}>
                <View style={{
                    borderRadius: 30,
                    overflow: 'hidden',
                    width: 32,
                    height: 32,
                    marginRight: 16,
                }}>
                    <Image
                        source={imgSource}
                        style={{
                            width: 32,
                            height: 32,
                            ...imgStyle
                        }}
                        resizeMode="stretch"
                    />
                </View>
                <View>
                    <Text style={{
                        fontFamily: 'avenir-black'
                    }}>{subtitle}</Text>
                    <ColoredRating rankValue={rankValue} />
                </View>
                <View style={{
                    flex: 1,
                    width: '100%',
                }}>
                    <Text style={{
                        fontFamily: 'avenir-black',
                        textAlign: 'right',
                    }}>{category}</Text>
                </View>

            </View>
            <View style={{
                flexDirection: 'row',
                flex: 1,
                alignItems: 'center',
            }}>
                <View style={{
                    flex: 1,
                    flexGrow: 1,
                    paddingRight: 8,
                }}>
                    <Text>{text}</Text>
                </View>
                {!readonly && (
                    <TouchableOpacity activeOpacity={0.5} style={{
                        width: 32,
                        height: 32,
                    }} onPress={onPress}>
                        <Image
                            source={rightImgSource}
                            style={{
                                width: 32,
                                height: 32,
                                ...imgStyle
                            }}
                            resizeMode="cover"
                        />
                    </TouchableOpacity>
                )}
            </View>
        </View>
        );
    }
}

const styles = StyleSheet.create({
})