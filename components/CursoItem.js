import React from 'react';
import { Alert, View, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { ListItem, Slider } from 'react-native-elements';
import * as constants from 'aula-upc/constants'
import Style from '../stylesheet'
import Text from './BaseText'

export default class CursoItem extends React.Component {
    render() {
        const {
            underlayColor = 'rgba(0,0,0,.05)',
            activeOpacity = 0.5,
            onPress,
            title,
            imgSource = constants.DEFAULT_ICON_CURSO,
            containerStyle = {},
            textStyle = {},
            imgStyle = {},
            itemStyle = {},
            Component = onPress ? TouchableOpacity : View
        } = this.props;

        return (
            <Component style={[styles.highlight, containerStyle]}
                activeOpacity={activeOpacity}
                onPress={onPress}
            >
                <View>
                    <View style={[styles.item, itemStyle]}>
                        <Image
                            source={imgSource}
                            defaultSource={constants.ATOMO_BG}
                            loadingIndicatorSource={constants.ATOMO_BG}
                            resizeMode="contain"
                            style={[{
                                width: '100%',
                            }, imgStyle]}
                        />
                    </View>
                    <Text style={[{
                        alignSelf: 'center',
                        textAlign: 'center',
                    }, textStyle]}>{title}</Text>
                </View>
            </Component>
        );
    }
}

const styles = StyleSheet.create({
    highlight: {
        flex: 1,
        paddingHorizontal: 8,
        paddingVertical: 8,
        maxWidth: '33.33333%',
    },
    item: {
        ...Style.StartMainGridItem,
    }
})