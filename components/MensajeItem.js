import React from 'react';
import { View, StyleSheet, TouchableHighlight, Image } from 'react-native';
import { ListItem, Slider } from 'react-native-elements';
import * as constants from '../constants'
import Style from '../stylesheet'
import Text from './BaseText'
import Rating from './Rating'
import {isNumber} from 'lodash'



export default class MensajeItem extends React.Component {
    render() {
        const {
            onPress,
            title,
            subtitle,
            titleProps,
            titleStyle,
            subtitleProps,
            subtitleStyle,
            imgSource = constants.DEFAULT_ICON_ALUMNO,
            ...remainingProps
        } = this.props;
        
        return (
            <View>
                <ListItem
                    onPress={onPress}
                    containerStyle={{
                        paddingVertical: 8,
                        paddingHorizontal: 0,
                    }}
                    leftElement={
                        <View style={{
                            borderRadius: 64,
                            overflow: 'hidden',
                        }}>
                            <Image
                                source={imgSource}
                                style={{
                                    width: 48,
                                    height: 48,
                                }}
                                resizeMode="stretch"
                            />
                        </View>
                    }
                    rightElement={
                        <View
                            style={{
                                width: 16,
                                height: 16,
                                backgroundColor: constants.COLOR_CHEVRON,
                                borderRadius: 16,
                            }}
                        />
                    }
                    title={<Text
                        style={titleStyle}
                        {...titleProps}>
                        {title}
                    </Text>
                    }
                    subtitle={subtitle && (
                        <View>
                            <Text
                                style={subtitleStyle}
                                {...subtitleProps}
                            >
                                {subtitle}
                            </Text>
                        </View>
                    )}

                    {...remainingProps}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    highlight: {
        flex: 1,
        paddingHorizontal: 8,
        paddingVertical: 8,
    },
    item: {
        ...Style.StartMainGridItem,
    }
})