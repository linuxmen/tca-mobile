import React from 'react';
import {
    StyleSheet,
    View,
    Image,
    ActivityIndicator,
    Dimensions,
} from 'react-native';


import Text from './BaseText';

import * as constants from '../constants';

export default class Loading extends React.Component {
    render() {
        var mensaje = this.props.message || 'Cargando...';
        var logo = this.props.logo || true;
        return (
            <View style={styles.cargandoStyles}>
                <ActivityIndicator 
                    size="large" 
                    color={constants.COLOR_PRIMARY}
                    />
                <Text h6 style={{
                    textAlign: 'center', 
                    alignSelf: 'center'
                }}>{mensaje}</Text>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    cargandoStyles: {
        backgroundColor: constants.bgColor,
        height: '100%',
        alignContent: 'center',
        justifyContent: 'center',
        width: '100%',
        // zIndex: 100, // para tapar toda la pantalla
    },
    logoWrapper:{ 
        backgroundColor: 'white', 
        alignItems: 'center',
        justifyContent: 'center', 
        width: '100%',
        height: '30%',
        minHeight: 115
    },
    logoImg: {
        width: '100%',
        paddingHorizontal: 20,
        alignSelf: 'center',
    },
});


